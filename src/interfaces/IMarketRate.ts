export interface IMarketRate {
    market: {
        btc: string,
        ltc: string,
        eth: string,
        usdt: string,
    }
}