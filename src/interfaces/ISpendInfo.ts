export interface ISpendInfo {
    info: {
        all: {
            all: string,
            today: string,
            yesterday: string,
        }
        available: {
            all: string,
            btc: string,
            ltc: string,
            eth: string,
            usdt: string,
        }
        invested: {
            all: string,
            daySummary: string,
            weekSummary: string,
            monthSummary: string,
            yearSummary: string,
        }
        summary: {
            active: string,
            plans: number,
            allSummary: string,
            participationTime: string,
            withdrawnMoney: string,
        }
    }
}