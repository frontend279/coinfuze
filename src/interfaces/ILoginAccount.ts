export interface ILoginAccount {
    login: string;
    password: string;
}

export interface IChangePassword {
    previousPassword: string,
    currentPassword: string,
}