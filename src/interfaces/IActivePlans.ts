import {bool} from "yup";

export interface IActivePlans {
    id: number;
    createdAt: string;
    isActive: boolean;
    minInvest: number;
    time: string;
    waitingFinanceResult: number;
    startDate: string;
    endDate: string;
    startFund?: number;
    addedFund?: number;
    financeResult?: number;
    profit?: number;
    investedMoney?: number;
    planCurrency?: string;
    status: string;
}

export interface IInvestings {
    id: number;
    createdAt: string;
    investPlansId: number;
    user: {
        login: string;
    };
    userId: number;
    money?: number;
    planCurrency?: string;
    isActivated: boolean;
}

export interface IAdminPlan extends IActivePlans {
    id: number;
    Investings: IInvestings[]
}

export interface IAdminPlans {
    id: number;
}

export interface IActiveInvesting {
    id: number;
    plan: IActivePlans,
    money?: number,
    planCurrency: string,
    isActivated: boolean,
}

export interface IActivePlanCreate {
    minInvest: number;
    waitingFinanceResult: number;
    startDate: string;
    endDate: string;
    planCurrency: string;
}

export interface IActivePlanEdit extends Partial<IActivePlanCreate> {
    id: number;
}

export interface IActivePlanInvest {
    id: number;
    summary: number;
}

export interface IAvailablePlan {
    id: number,
    minInvest: number;
    waitingFinanceResult: number;
    startDate: string;
    endDate: string;
    planCurrency: string;
}