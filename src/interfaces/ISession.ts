export interface ISession {
    id: number,
    createdAt: string,
    browser: string,
    userAgent: string,
    ip: string,
    os: string,

}