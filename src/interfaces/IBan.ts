export interface IBan {
    id: number;
    createdAt: string;
    updatedAt: string;
    adminId: number;
    userId: number;
    reason: string;
    startDate: string;
    expirationDate: string;
}