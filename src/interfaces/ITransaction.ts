export interface ITransaction {
    id: number;
    createdAt: string;
    updateAt: string;
    userId: string;
    action: string;
    currency: string;
    amount: number;
    comment: string;
    status: string;
    getByUserID: number;
}