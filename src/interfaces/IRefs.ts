export interface IRefs {
    regDate: string;
    userName: string;
    ID: number;
    partnerAmount: number;
    depositSummary: string;
    plansSummary: string;
    allSummary: string;
}