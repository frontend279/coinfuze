import {ITransaction} from "./ITransaction";
import {IBan} from "./IBan";

export interface IAccountInfo extends IAllData {
    login: string,
    email: string,
}

export interface IAccountAddData {
    avatar: string,
    lang: string,
    verified: boolean,
}

export interface IAccountData {
    city: string,
    country: string,
    addressFirst: string,
    addressLast: string,
    firstName: string,
    lastName: string,
    postalCode: string,
    birthday: string,
    createdAt: string,
    phone: string,
    middleName: string,
}

export interface IAllData extends IAccountData, IAccountAddData {

}

export interface IAuthPayload extends IAuthTokens, IAccountInfo{}



export interface IAuthTokens {
    access_token: string,
    refresh_token: string,
}

export interface ITokenInfo {
    sub: number,
    login: string,
    role: string,
}

export interface IUpdateAccountInfo {
    email?: string,
    userInfo: Partial<IAccountInfo>,
}

export interface IAdminAccounts {
    id: number,
    createdAt: string,
    login: string,
    role: string,
    email: string,
    userInfo: {
        avatar: string,
        phone: string,
    }
}

export interface IAdminAccount {
    id: number,
    createdAt: string,
    login: string,
    role: string,
    email: string,
    userInfo: IAllData,
    transactions: ITransaction[],
    Bans: IBan[],
}