export interface ICreateAccount {
    login: string,
    password: string,
    email: string,
    userInfo: {
        phone: string,
        city: string,
        country: string,
        addressFirst: string,
        addressLast: string,
        firstName: string,
        lastName: string,
        postalCode: string,
        middleName: string,
        birthday: string,
    }
}

export interface IValidateRegisterPayload {
    validate: boolean,
}

export interface IValidateRegisterData {
    login: string,
    email: string,
}

