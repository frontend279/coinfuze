import {combineReducers, configureStore} from "@reduxjs/toolkit";
import {TypedUseSelectorHook, useDispatch, useSelector} from "react-redux";
import {CompleteRegSlice, nameCompleteRegSlice} from "./slices/others/CompleteRegSlices";
import {api, protectedAPI} from "../API/API";
import {AuthModalSlice, nameAuthModalSlice} from "./slices/others/AuthModalSlice";
import {nameUserDataSlice, UserDataSlice} from "./slices/user/UserDataSlice";


const rootReducer = combineReducers({
    [nameCompleteRegSlice]: CompleteRegSlice.reducer,
    [nameAuthModalSlice]: AuthModalSlice.reducer,
    [nameUserDataSlice]: UserDataSlice.reducer,
    [api.reducerPath]: api.reducer,
    [protectedAPI.reducerPath]: protectedAPI.reducer,
})

export const store = configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(protectedAPI.middleware, api.middleware),
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export const useAppDispatch: () => AppDispatch = useDispatch
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
