import {createSlice, PayloadAction} from "@reduxjs/toolkit";

export const nameAuthModalSlice = 'AuthModalSlice'

interface IAuthModalSlice {
    isShown: boolean
}

const initialState: IAuthModalSlice = {
    isShown: false
}

export const AuthModalSlice = createSlice({
    name: nameAuthModalSlice,
    initialState,
    reducers: {
        setShown: (state, action: PayloadAction<boolean>) => {
            state.isShown = action.payload
        }
    },
    extraReducers: {},
})

export const AuthModalActions = AuthModalSlice.actions