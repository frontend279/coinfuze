import {createSlice, PayloadAction} from "@reduxjs/toolkit";

export const nameCompleteRegSlice = 'CompleteRegSlice'

interface ICompleteRegSlice {
    isShown: boolean
}

const initialState: ICompleteRegSlice = {
    isShown: false
}

export const CompleteRegSlice = createSlice({
    name: nameCompleteRegSlice,
    initialState,
    reducers: {
        setShown: (state, action: PayloadAction<boolean>) => {
            state.isShown = action.payload
        }
    },
    extraReducers: {},
})

export const CompleteRegActions = CompleteRegSlice.actions
