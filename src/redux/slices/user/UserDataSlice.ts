import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {IAccountInfo, IAuthTokens} from "../../../interfaces/IAccountInfo";
import {setTokens} from "../../../utils/Tokens";
import {protectedAPI} from "../../../API/API";

export const nameUserDataSlice = 'UserDataSlice'



export interface IUserDataSlice {
    accountInfo: IAccountInfo,
    tokens: IAuthTokens,
    isAdmin: boolean,
    isAuth: boolean,

}

const initialState: IUserDataSlice = {
    accountInfo: {} as IAccountInfo,
    tokens: {} as IAuthTokens,
    isAdmin: false,
    isAuth: false,
}

export const UserDataSlice = createSlice({
    name: nameUserDataSlice,
    initialState,
    reducers: {
        setUserData: (state, action: PayloadAction<IAccountInfo> ) => {
            state.accountInfo = action.payload;
        },
        setUserToken: (state, action: PayloadAction<IAuthTokens> ) => {
            setTokens(action.payload.access_token, action.payload.refresh_token);
            state.tokens = action.payload;
        },
        setIsAdmin: (state, action: PayloadAction<boolean> ) => {
            state.isAdmin = action.payload;
        },
        setIsAuth: (state, action: PayloadAction<boolean> ) => {
            state.isAuth = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addMatcher(
            protectedAPI.endpoints?.logout.matchFulfilled,
            (state, { payload }) => {
                state.isAuth = initialState.isAuth;
                state.tokens = initialState.tokens;
                state.accountInfo = initialState.accountInfo;
                state.isAdmin = initialState.isAdmin;
                setTokens("", "");
            }
        );
    }
})



export const UserDataActions = UserDataSlice.actions

