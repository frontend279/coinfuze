import {createTheme} from "@mui/material";
import {green} from "@mui/material/colors";
import {ruRU} from '@mui/material/locale';

export let theme = createTheme({
	palette: {
		mode: "dark",
		primary: {
			main: "#ff0000",
			contrastText: "#fff",
		},
		secondary: {
			main: green[500],
		},
	},

}, ruRU);