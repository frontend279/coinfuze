import News from "./News/News";
import ANews from "./AdminPages/News/News";
import {Route, Routes} from "react-router-dom";
import NewsCurrent from "./NewsCurrent/NewsCurrent";
import Withdraw from "./Withdraw/Withdraw";
import WithdrawProceed from "./WithdrawProceed/WithdrawProceed";
import Partner from "./Partner/Partner";
import Profile from "./Profile/Profile";
import Deposit from "./Deposit/Deposit";
import DepositProceed from "./DipositProceed/DepositProceed";
import Main from "./Main/Main";
import PlanManage from "./PlanManage/PlanManage";
import {useCookies} from "react-cookie";
import {useAppDispatch, useAppSelector} from "../redux";
import {useEffect, useState} from "react";
import {AuthModalActions} from "../redux/slices/others/AuthModalSlice";
import Users from "./AdminPages/Users/Users";
import {decodeToken, getTokens} from "../utils/Tokens";
import {useCheckMutation} from "../API/API";
import {UserDataActions} from "../redux/slices/user/UserDataSlice";
import Plans from "./AdminPages/Plans/Plans";


const adminPages = [
	{
		path: '/admin/users',
		element: <Users />,
		id: 'admin/users'
	},
	{
		path: '/admin/plans',
		element: <Plans />,
		id: 'admin/plans'
	},
	{
		path: '/admin/news',
		element: <ANews />,
		id: 'admin/plans'
	},
]

const privatePages = [
	{
		path: '/news',
		element: <News />,
		id: 'news'
	},
	{
		path: '/news/:id',
		element: <NewsCurrent />,
		id: 'newsId'
	},  
	{
		path: '/withdraw',
		element: <Withdraw />,
		id: 'withdraw'
	},
	{
		path: '/withdraw/proceed',
		element: <WithdrawProceed />,
		id: 'withdrawProceed'
	},
	{
		path: '/partner',
		element: <Partner />,
		id: 'partner'
	},
	{
		path: '/profile',
		element: <Profile />,
		id: 'profile'
	},
	{
		path: '/deposit',
		element: <Deposit />,
		id: 'deposit'
	},
	{
		path: '/deposit/proceed',
		element: <DepositProceed />,
		id: 'depositProceed'
	},
	{
		path: '/',
		element: <Main />,
		id: 'main'
	},
	{
		path: '/plan',
		element: <PlanManage />,
		id: 'planManage'
	},
]



export const Router = () => {
	const [cookies, setCookies] = useCookies(["access_token", "refresh_token"])
	const userData = useAppSelector(selector => selector.UserDataSlice)
	const [check, {error, data}] = useCheckMutation();
	const dispatch = useAppDispatch();
	const [isAuth, setIsAuth] = useState(false)
	const [isAdmin, setAdmin] = useState(false)


	const setAuthenticated = (auth: boolean, admin: boolean = false) => {
		dispatch(UserDataActions.setIsAuth(auth))
		dispatch(AuthModalActions.setShown(!auth));
		dispatch(UserDataActions.setIsAdmin(admin))
	}

	const checkForAdmin = () => {
		if(decodeToken().role === "ADMIN") {
			setAuthenticated(true, true)
		} else {
			setAuthenticated(true)
		}
	}

	useEffect(() => {
		if(error) {
			setAuthenticated(false)
		} else if(data) {
			dispatch(UserDataActions.setUserData(data))
			dispatch(UserDataActions.setUserToken({
				access_token: getTokens().access_token,
				refresh_token: getTokens().refresh_token,
			}))
			checkForAdmin()
		}
	}, [error, data, ]);

	useEffect(() => {
		if(userData.tokens.refresh_token && userData.tokens.access_token) {
			checkForAdmin()
		} else {
			setAuthenticated(false)
		}
	}, [userData, ]);


	useEffect(() => {
		if(!getTokens().access_token) {
			setAuthenticated(false);
		} else {
			check();
		}
	}, []);


	return (
		<Routes>
			{
				userData.isAuth
				&&
				privatePages.map((page, index) =>
					<Route key={index}  {...page}></Route>)
			}
			{
				userData.isAdmin
				&&
				adminPages.map((page, index) =>
					<Route key={index} {...page}></Route>)
			}
		</Routes>
	)
}

