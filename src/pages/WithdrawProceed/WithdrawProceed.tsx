import React, {FC, useState} from 'react';
import {Stack} from "@mui/material";
import {WithdrawProceedStack} from "../../components/WithdrawProceed/styles";
import ChooseBalance from "../../components/WithdrawProceed/ChooseBalance";
import ChooseWallet from "../../components/WithdrawProceed/ChooseWallet";
import WithdrawProceedInfo from "../../components/WithdrawProceedInfo/WithdrawProceedInfo";
import PageHead from "../../components/PageHead/PageHead";

const WithdrawProceed: FC = () => {
	const [state, setState] = useState(1)
	
	const loadingPage = () => {
		switch (state) {
			case 1:
				return <ChooseBalance setState={setState} />
			case 2:
				return <ChooseWallet setState={setState} />
			default:
				return <ChooseBalance setState={setState} />
		}
	}
	
	return (
		<Stack flex={1} direction={'column'} p={2} pr={4}>
			<PageHead title={'Вывод средств'} />
			<WithdrawProceedStack gap={2} p={2} mt={2} direction={'column'}>
				{
					loadingPage()
				}
				<WithdrawProceedInfo />
			</WithdrawProceedStack>
		</Stack>
	)
};

export default WithdrawProceed;