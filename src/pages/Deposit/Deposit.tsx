import React, {FC} from 'react';
import {Stack} from "@mui/material";
import PageHead from '../../components/PageHead/PageHead';
import EarnWithUs from "../../components/DepositCards/EarnWithUs/EarnWithUs";
import DepositCard from "../../components/DepositCards/Deposit/DepositCard";
import DepositTable from "../../components/DepositTable/DepositTable";

const Deposit: FC = () => {
	return (
		<Stack flex={1} direction={'column'} p={2} pr={4}>
			<PageHead title={'Депозит'} />
			<Stack mt={2} direction={'row'} justifyContent={'space-between'} gap={1}>
				<DepositCard />
				<EarnWithUs />
			</Stack>
			<DepositTable />
		</Stack>
	);
};

export default Deposit;