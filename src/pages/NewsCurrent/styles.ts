
import {Box, Stack, styled} from "@mui/material";

export const NewsBox = styled(Box)(({theme}) => ({
	background: theme.palette.background.default,
	color: theme.palette.text.primary,
	padding: '0.5rem 1rem',
	width: '100%',
}))

export const TopBarBox = styled(Box)(({theme}) => ({
	display: 'flex',
	flexDirection: 'row',
	alignItems: 'center',
	justifyContent: 'space-between',
}))


export const TopBarNavBox = styled(Box)(({theme}) => ({
	display: 'flex',
	flexDirection: 'row',
	alignItems: 'center',
	gap: '1rem',
}))

export const TopBarSortBox = styled(Box)(({theme}) => ({
	display: 'flex',
	flexDirection: 'row',
	alignItems: 'center',
	gap: '1rem',
}))

export const ContentBox = styled(Box)(({theme}) => ({
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
	justifyContent: 'space-between',
}))

export const NewsCurrentBox = styled(Box)(({theme}) => ({
	display: 'flex',
	flexDirection: 'column',
	backgroundColor: 'rgb(206,237,245)',
	color: 'black',
	gap: '1rem',
	padding: '1rem',
	marginTop: '1rem'
}))

export const NewsCurrentContentBox = styled(Box)(({theme}) => ({
	display: 'flex',
	flexDirection: 'row',
	alignItems: 'center',
	gap: '1.5rem',
	padding: '0.5rem 1.5rem',
}))