import React, {FC} from 'react';
import {
	Box, Divider,
	IconButton,
	MenuItem,
	Select,
	SelectChangeEvent,
	Stack,
	Typography
} from "@mui/material";
import Navbar from "../../components/Navbar/Navbar";
import LeftMenu from "../../components/LeftMenu/LeftMenu";
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';
import ArrowCircleRightIcon from '@mui/icons-material/ArrowCircleRight';
import {
	ContentBox,
	NewsBox,
	NewsCurrentBox,
	NewsCurrentContentBox,
	TopBarBox,
	TopBarNavBox,
	TopBarSortBox
} from "./styles";

const NewsCurrent: FC = () => {
	const [newsSortVariant, setNewsSortVariant] = React.useState('10');

	const handleChange = (event: SelectChangeEvent) => {
		setNewsSortVariant(event.target.value as string);
	};

	

	return (
				<NewsBox>
					<Stack direction="column">
						<TopBarBox>
							<TopBarNavBox>
								<Typography variant={'h6'}>Новости</Typography>
								<Box>
									<IconButton>
										<ArrowCircleLeftIcon/>
									</IconButton>
									<IconButton>
										<ArrowCircleRightIcon/>
									</IconButton>
								</Box>
							</TopBarNavBox>
							<TopBarSortBox>
								<Typography>Сортировать по</Typography>
								<Select
									value={newsSortVariant}
									label="Сортировка"
									onChange={handleChange}
									sx={{ width: 120, borderRadius: '1rem' }}
								>
									<MenuItem value={10}>Дате</MenuItem>
								</Select>
							</TopBarSortBox>
						</TopBarBox>
						<ContentBox>
							<NewsCurrentBox>
								<Typography variant={'h5'}>21 Февраль 2022 | Сайт был создан</Typography>
								<Divider color={'black'} />
								<NewsCurrentContentBox>
									<img alt={'news image'} height={300} src={'https://res.cloudinary.com/startup-grind/image/upload/c_fill,dpr_2.0,f_auto,g_center,h_1080,q_100,w_1080/v1/gcs/platform-data-byu/events/Open-For-Business_03_dtJkdGa.jpg'}></img>
									<Typography>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at volutpat eros. Vivamus ex leo, dignissim eu gravida eu, ornare sit amet lorem. Nulla aliquet lacus a sem consectetur, sit amet fringilla urna finibus. Morbi gravida feugiat odio. Cras eget nisi justo. Curabitur vel interdum augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis a pretium ex.

										Morbi porttitor placerat ligula. Proin in quam tincidunt, facilisis justo eu, dapibus lorem. Quisque eget tempor velit. Phasellus ullamcorper faucibus erat, sed venenatis arcu pharetra eu. Duis vestibulum diam id dapibus condimentum. Donec id metus tincidunt, tincidunt ante vel, porta est. Sed sit amet leo elit. Nullam eros augue, dignissim condimentum risus non, gravida tincidunt leo. Praesent aliquam nisi sit amet massa maximus dignissim. Donec dignissim placerat rutrum. Mauris maximus hendrerit massa, in tincidunt ligula sollicitudin eu. Aenean elementum eros ut tellus lobortis, faucibus egestas mi fermentum. Quisque gravida enim sed ipsum venenatis, vitae bibendum neque congue. Vestibulum interdum dui vitae libero lobortis, et tristique dolor semper. Nullam quis neque augue. Maecenas suscipit arcu eu mauris iaculis, aliquet egestas arcu vulputate.

										Donec facilisis finibus lectus, vel imperdiet augue convallis vitae. Fusce sed velit ut tellus tempus tincidunt. Vivamus condimentum nunc ac massa blandit, in vehicula massa mattis. Quisque aliquet nisi elit. Ut quis nulla lorem. Nunc porta varius venenatis. Proin quam urna, vulputate at molestie a, placerat venenatis tortor. Maecenas sit amet ligula nulla.
									</Typography>
								</NewsCurrentContentBox>
							</NewsCurrentBox>		
						</ContentBox>
					</Stack>
				</NewsBox>
	);
};

export default NewsCurrent