import React, {FC, useEffect} from 'react';
import {Stack} from "@mui/material/";
import CompleteRegisterAlert from "../../components/CompleteRegisterAlert/CompleteRegisterAlert";
import {useDispatch} from "react-redux";
import {useAppSelector} from "../../redux";
import {CompleteRegActions} from "../../redux/slices/others/CompleteRegSlices";
import PageHead from "../../components/PageHead/PageHead";
import MainPageCards from "../../components/MainPageCards/MainPageCards";

const Main: FC = () => {

	const isShown = useAppSelector(state => state.CompleteRegSlice.isShown)
	const dispatch = useDispatch()
	useEffect(() => {
		//TODO: запрос к серверу
		dispatch(CompleteRegActions.setShown(true))
	}, [])

	return (
		<Stack gap={2} flex={1} direction={'column'} p={2} pr={4}>
			<PageHead title={'Панель управления'} />
			<CompleteRegisterAlert isShown={isShown} />
			<MainPageCards />
		</Stack>
	);
};

export default Main;