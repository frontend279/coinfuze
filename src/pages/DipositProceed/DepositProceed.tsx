import React, {FC} from 'react';
import { Stack} from "@mui/material";
import PageHead from "../../components/PageHead/PageHead";
import DepositTable from "../../components/DepositTable/DepositTable";
import DepositProceedMain from '../../components/DepositProceedMain/DepositProceedMain';

const DepositProceed: FC = () => {
	return (
		<Stack flex={1} direction={'column'} p={2} pr={4}>
			<Stack flex={1} direction={'column'}>
				<PageHead title={'Депозит'} />
				<DepositProceedMain />
			</Stack>
			<DepositTable />
		</Stack>
	);
};

export default DepositProceed;