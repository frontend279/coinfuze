import React, {FC} from 'react';
import PageHead from "../../components/PageHead/PageHead";
import PartnerProgram from "../../components/PartnerProgram/PartnerProgram";
import PartnerRefUrl from "../../components/PartnerRefUrl/PartnerRefUrl";
import PartnerTables from "../../components/PartnerTables/PartnerTables";
import {Stack} from "@mui/material";

const Partner: FC = () => {
	return (
		<Stack flex={1} direction={'column'} p={2} pr={4}>
			<PageHead title={'Партнерская программа'} />
			<Stack  gap={2} mt={2} direction={'column'}>
				<Stack gap={2} direction={'row'}>
					<PartnerProgram />
					<PartnerRefUrl />
				</Stack>
				<PartnerTables />
			</Stack>
		</Stack>
	);
};

export default Partner;