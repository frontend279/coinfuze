import React, {FC, useEffect, useId, useState} from 'react';
import {Avatar, Button, Divider, Stack, Typography} from "@mui/material";
import {InputTextField, LoadAvatarText} from "./styles";
import * as Yup from "yup";
import {useFormik} from "formik";
import {useAppDispatch, useAppSelector} from "../../redux";
import {AdapterDateFns} from "@mui/x-date-pickers/AdapterDateFnsV3";
import {DateField, LocalizationProvider} from "@mui/x-date-pickers";
import {useUpdateUserDataMutation} from "../../API/API";
import {getAvatarURL} from "../../utils/Utils";
import {UserDataActions} from "../../redux/slices/user/UserDataSlice";

const ProfileData: FC = () => {

	const id = useId()
	const dispatch = useAppDispatch();

	const [UpdateUser, {data}] = useUpdateUserDataMutation()

	useEffect(() => {
		if(data) {
			dispatch(UserDataActions.setUserData(data))
		}
	}, [data, ]);

	const [avatar, setAvatar] = useState<File>()

	const userData = useAppSelector(selector => selector.UserDataSlice.accountInfo)

	const validateSchema = Yup.object().shape({
		firstName: Yup.string().required("Это поле обязательно"),
	});

	const formik = useFormik({
		initialValues: {
			firstName: userData.firstName,
			lastName: userData.lastName,
			birthday: userData.birthday,
			postalCode: userData.postalCode,
			addressFirst: userData.addressFirst,
			addressLast: userData.addressLast,
			city: userData.city,
			country: userData.country,
			phone: userData.phone,
			middleName: userData.middleName,
		},
		validationSchema: validateSchema,
		validateOnChange: false,
		onSubmit: (values, { resetForm }) => {
			const formData = new FormData();
			for (let value in values) {
				formData.append(`userInfo[${value}]`, values[value as keyof typeof values]);
			}
			if(avatar !== undefined)
				formData.append('file', avatar);
			UpdateUser(formData)
		},
	});

	const getAvatar = () => {
		if(avatar) {
			return URL.createObjectURL(avatar)
		} else {
			return getAvatarURL(userData.avatar);
		}
	};

	const loadAvatar = (e: React.ChangeEvent<HTMLInputElement>) => {
		if(e.target.files?.length)
			setAvatar(e.target.files[0])
	}


	return (
		<form onSubmit={formik.handleSubmit}>
			<Stack p={3} direction={'row'}>
				<Stack direction={'column'}>
					<Avatar
						alt={userData.login}
						src={getAvatar()}
						sx={{width: 120, height: 120}}
					/>
					<label htmlFor={id}>
						<LoadAvatarText mt={1}  variant={'body1'}>Изменить аватар
							<input id={id} accept="image/*" style={{ display: 'none' }} type="file" onChange={loadAvatar}/>
						</LoadAvatarText>
					</label>


				</Stack>
				<Stack ml={2} direction={'column'}>
					<Typography variant={'h6'}>{userData.firstName} {userData.lastName}</Typography>
					<Typography variant={'body1'}>Участник с {userData.createdAt}</Typography>
				</Stack>
			</Stack>
			<Divider color={'black'} />
			<Stack gap={2} py={1} px={3} direction={'row'}>
				<Stack gap={1} flex={1} direction={'column'}>
					<InputTextField
						label="Ваше имя"
						value={formik.values.firstName}
						onChange={formik.handleChange}
						name="firstName"
					/>
					<InputTextField
						label="Ваше отчество"
						value={formik.values.middleName}
						onChange={formik.handleChange}
						name="middleName"
					/>
					<LocalizationProvider dateAdapter={AdapterDateFns}>
						<DateField
							disableFuture
							label="Дата рождения"
							format="dd/MM/yyyy"
							value={formik.values.birthday}
							onChange={(value) => formik.setFieldValue("birthday", value, false)}
							slotProps={{
								textField: {
									variant: "outlined",
									error: !!formik.errors.birthday,
									helperText: formik.errors.birthday,
									sx: {
										borderRadius: '2rem',
										'& .MuiOutlinedInput-notchedOutline': {
											borderColor: "common.black",
										},
										'& .MuiFormLabel-root': {
											color: "common.black",
										}
									}
								}
							}}
						/>
					</LocalizationProvider>
					{/*<InputTextField*/}
					{/*	label="Дата рождения"*/}
					{/*	value={formik.values.birthday}*/}
					{/*	onChange={formik.handleChange}*/}
					{/*	name="birthday"*/}
					{/*/>*/}
					<InputTextField
						label="Адрес строка"
						value={formik.values.addressFirst}
						onChange={formik.handleChange}
						name={"addressF"}
					/>

					<InputTextField
						label="Город"
						value={formik.values.city}
						onChange={formik.handleChange}
						name={'city'}
					/>
				</Stack>
				<Stack gap={1} flex={1} direction={'column'}>
					<InputTextField
						label="Ваша фамилия"
						value={formik.values.lastName}
						onChange={formik.handleChange}
						name={'lastName'}
					/>
					<InputTextField
						label="Номер телефона"
						value={formik.values.phone}
						onChange={formik.handleChange}
						name="phone"
					/>
					<InputTextField
						label="Почтовый индекс"
						value={formik.values.postalCode}
						onChange={formik.handleChange}
						name={'postalCode'}
					/>
					<InputTextField
						label="Адресная строка 2"
						value={formik.values.addressLast}
						onChange={formik.handleChange}
						name={'addressL'}
					/>
					<InputTextField
						label="Страна"
						value={formik.values.country}
						onChange={formik.handleChange}
						name={'country'}
					/>
				</Stack>
			</Stack>
			<Stack mt={1} mb={3} px={3} justifyContent={'center'} gap={2} direction={'row'}>
				<Button type={'reset'}
						onClick={formik.handleReset}
						variant="contained"
						sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
							padding: '0 3rem',}}
						color={'error'}>
					Отмена
				</Button>
				<Button
					type={'submit'}
					variant="contained"
					sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
							padding: '0 3rem',}}
					color={'success'}>Сохранить</Button>

			</Stack>
		</form>
	);
};

export default ProfileData;