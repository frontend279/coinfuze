import React, {FC} from 'react';
import {Box, Button, Modal, Stack, TextField, Typography} from "@mui/material";
import {ExitBox, InputTextField} from "./styles";
import CloseIcon from '@mui/icons-material/Close';

const ProfileExitModal: FC = () => {
	return (
		<Modal
			open={false}
			onClose={() => {}}
		>
			<ExitBox>
				<Stack direction={'row'} justifyContent={'end'}>
					<CloseIcon sx={{width:'40px', height: '40px'}} />
				</Stack>
				<Typography textAlign={'center'} variant="h6" component="h2">
					Text in a modal
				</Typography>
				<Typography sx={{ mt: 2 }}>
					Пожалуйста, введите свой пароль, чтобы подтвердить, что вы хотите выйти из других сеансов
					браузера на всех ваших устройствах.
				</Typography>
				<InputTextField
					sx={{ mt: 2 }}
					label="Пароль"
					type="password"
					autoComplete="current-password"
					fullWidth
				/>
				<Stack mt={2} gap={2} direction={'row'} justifyContent={'center'}>
					<Button variant="contained"
									sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
										padding: '0 3rem',}}
									color={'info'}>
						Отмена
					</Button>
					<Button variant="contained"
									sx={{borderRadius: '20px', maxWidth: '600px', minHeight: '40px',
										padding: '0 3rem',}}
									color={'error'}>
						Выйти из других сеансов браузера
					</Button>
				</Stack>
			</ExitBox>
		</Modal>
	);
};

export default ProfileExitModal;