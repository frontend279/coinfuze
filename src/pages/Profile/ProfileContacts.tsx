import React, {FC, useEffect, useState} from 'react';
import {Button, MenuItem, Stack} from "@mui/material";
import {InputTextField, LangChooseInputBase, LangChooseSelect} from "./styles";
import {useAppDispatch, useAppSelector} from "../../redux";
import {useUpdateUserDataMutation} from "../../API/API";
import {SerializedError} from "@reduxjs/toolkit";
import * as Yup from "yup";
import {useFormik} from "formik";

const ProfileContacts: FC = () => {

	const locales = {
		russian: {
			name: "Русский",
			code: 0,
		},
		english: {
			name: "English",
			code: 1,
		},
	}

	const [localError, setLocalError] = useState("")
	const [isSuccess, setIsSuccess] = useState(false)
	const dispatch = useAppDispatch()
	const userData = useAppSelector(selector => selector.UserDataSlice.accountInfo)

	const [UpdateUser, {data, error}] = useUpdateUserDataMutation()

	useEffect(() => {
		const errorMsg = (error as SerializedError)?.message;
		if(errorMsg !== undefined) {
			setIsSuccess(false)
			setLocalError(errorMsg);
		}

	}, [error, ]);

	useEffect(() => {
		if(data) {
			setLocalError("")
			setIsSuccess(true)
		}
	}, [data])

	const phoneRegExp = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/

	const validateSchema = Yup.object().shape({
		phone: Yup.string().required("Это поле обязательно").matches(phoneRegExp, 'Введите правильный номер телефона'),
		email: Yup.string().email("Введите настоящий email").required("Это поле обязательно"),
	});



	const formik = useFormik({
		initialValues: {
			email: userData.email,
			lang: userData.lang,
			phone: userData.phone,
		},
		validationSchema: validateSchema,
		validateOnChange: false,
		onSubmit: (values, { resetForm }) => {
			UpdateUser({
				email: values.email,
				userInfo: {
					lang: values.lang.toString(),
					phone: values.phone,
				}
			});
		},
	});
	
	return (
		<form style={{display: "flex", flexDirection: "column", padding: "1rem 2rem 0"}} onSubmit={formik.handleSubmit}>
			<Stack sx={{maxWidth: '50%'}} gap={2} p={3} direction={'column'}>
				<InputTextField
					label="Электронная почта"
					onChange={formik.handleChange}
					value={formik.values.email}
					name={"email"}
				/>
				<LangChooseSelect
					onChange={formik.handleChange}
					name={"lang"}
					value={+formik.values.lang}
					input={<LangChooseInputBase />}
				>
					<MenuItem disabled value={0}>
						Выберите язык
					</MenuItem>
					{
						Object.keys(locales).map((key, index) => (
							<MenuItem key={locales[key as keyof typeof locales].code} value={locales[key as keyof typeof locales].code}>{locales[key as keyof typeof locales].name}</MenuItem>
						))
					}
					
				</LangChooseSelect>
				<InputTextField
					onChange={formik.handleChange}
					name={"phone"}
					value={+formik.values.phone}
					label="Телефонный номер"
				/>
			</Stack>
			<Stack mt={1} mb={3} px={3} justifyContent={'center'} gap={2} direction={'row'}>
				<Button variant="contained"
						type={"reset"}
						onClick={formik.handleReset}
						sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
							padding: '0 3rem',}}
						color={'error'}>
					Отмена
				</Button>
				<Button variant="contained"
						type={'submit'}
						sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
							padding: '0 3rem',}}
						color={'success'}>
					Сохранить
				</Button>
			</Stack>
		</form>
	);
};

export default ProfileContacts;