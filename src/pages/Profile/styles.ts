import {Box, Modal, OutlinedInput, Select, Stack, styled, TextField, Typography} from "@mui/material";

export const ProfileMainStack = styled(Stack)({
	backgroundColor: '#c0aefa'	
})

export const InputTextField = styled(TextField)(({theme}) => ({
	borderRadius: '2rem',
	'& .MuiOutlinedInput-notchedOutline': {
		borderColor: theme.palette.common.black,		
	},
	'& .MuiFormLabel-root': {
		color: theme.palette.common.black,	
	}
}))

export const LangChooseInputBase = styled(OutlinedInput)(({theme}) => ({
	color: theme.palette.common.black,
	'& .MuiOutlinedInput-notchedOutline': {
		borderColor: theme.palette.common.black,
	},
}))

export const ActiveSessionsStack = styled(Stack)({
	backgroundColor: '#9be3fb'
})

export const LangChooseSelect = styled(Select)(({theme}) => ({
	'& .MuiSelect-icon': {
		color: theme.palette.common.black,
	}
}))

export const ExitBox = styled(Box)((theme) => ({
	position: 'absolute',
	top: '20%',
	left: '50%',
	maxWidth: 800,
	backgroundColor: '#00e676',
	padding: "1rem",
}))

export const LoadAvatarText = styled(Typography)((theme) => ({
	"&:hover": {
		color: "white",
		cursor: "pointer"
	}
}))