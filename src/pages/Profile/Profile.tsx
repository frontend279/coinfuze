import React, {FC, useEffect, useState} from 'react';
import {Button, Divider, Stack, Tab, Tabs, Typography} from "@mui/material";
import {ActiveSessionsStack, ProfileMainStack} from "./styles";
import ProfileData from "./ProfileData";
import ProfileContacts from "./ProfileContacts";
import ProfilePassword from "./ProfilePassword";
import ProfileTwoAuth from "./ProfileTwoAuth";
//import MonitorIcon from '@mui/icons-material/Monitor';
import ProfileExitModal from "./ProfileExitModal";
import PageHead from "../../components/PageHead/PageHead";
import SessionCloseButton from "../../components/SessionCloseButton/SessionCloseButton";
import {useGetSessionsQuery, useRemoveOtherSessionsMutation} from "../../API/API";
import {ISession} from "../../interfaces/ISession";

const Profile: FC = () => {

	const [tab, setTab] = useState<number>(0)

	const {data} = useGetSessionsQuery();
	const [RemoveOtherSessions, {dataSessions, error}] = useRemoveOtherSessionsMutation();

	const [sessions, setSessions] = useState<ISession[]>([])


	useEffect(() => {
		if(data) {
			setSessions(data);
		}
	}, [data, ]);

	useEffect(() => {
		if(dataSessions) {
			setSessions(dataSessions);
		}
	}, [dataSessions, ]);


	const handleRemoveAllSessions = () => {

	}

	const showTab = () => {
		switch (tab) {
			case 0:
				return <ProfileData/>
			case 1:
				return <ProfileContacts/>
			case 2:
				return <ProfilePassword/>
			case 3:
				return <ProfileTwoAuth/>
			default:
				return <ProfileData/>
		}
	}

	return (
		<>
			<Stack flex={1} direction={'column'} p={2} pr={4}>
				<PageHead title={"Профиль"} />
				<ProfileMainStack mt={2} direction={"column"}>
					<Tabs value={tab}>
						<Tab onClick={() => setTab(0)} label="Личные данные"/>
						<Tab onClick={() => setTab(1)} label="Контакты"/>
						<Tab onClick={() => setTab(2)} label="Пароль"/>
						<Tab onClick={() => setTab(3)} label="Двухфакторная аутентификация"/>
					</Tabs>
					{
						showTab()
					}
				</ProfileMainStack>
				<ActiveSessionsStack mt={3} direction={'column'}>
					<Stack direction={'row'} my={2} px={3}>
						<Typography variant={'h6'}>Активные сессии</Typography>
					</Stack>
					<Divider color={'black'}/>
					<Stack mt={2} gap={6} px={3} direction="row">
						<Stack flex={1} gap={2} direction={'column'}>
							<Typography variant={'body1'}>Сеансы браузера</Typography>
							<Typography variant={'body2'}>Управляйте своими активными сеансами в других браузерах
								и устройствах и выходите из них</Typography>
							<Stack direction={"column"}>
								{
									sessions.length !== 0 && sessions.map((session) => (
										<SessionCloseButton key={session.id} session={session} setSession={setSessions}/>
									))
								}
							</Stack>
						</Stack>
						<Stack flex={1} direction={'column'}>
							<Typography variant={'body2'}>При необходимости вы можете выйти из всех других сеансов браузера
								на всех своих устройствах. Некоторые из ваших недавних сеансов перечислены ниже, однако этот
								список может быть не исчерпывающим. Если вы считаете, что ваша учетная запись была скомпрометирована,
								вам также следует обновить пароль</Typography>

							<Button variant="contained"
											sx={{
												borderRadius: '20px', maxWidth: '400px', minHeight: '40px',
												padding: '0 3rem', margin: '1rem 0'
											}}
											color={'info'}
											onClick={handleRemoveAllSessions}
							>
								Выход из других сеансов браузера
							</Button>
						</Stack>
					</Stack>
				</ActiveSessionsStack>
			</Stack>
			<ProfileExitModal/>
		</>
	);
};

export default Profile;