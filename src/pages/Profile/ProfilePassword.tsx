import React, {FC, useEffect, useState} from 'react';
import {Button, Stack, Typography} from "@mui/material";
import {InputTextField} from "./styles";
import {useAppDispatch} from "../../redux";
import {useChangePasswordMutation} from "../../API/API";
import {SerializedError} from "@reduxjs/toolkit";
import * as Yup from "yup";
import {useFormik} from "formik";

const ProfilePassword: FC = () => {

	const [localError, setLocalError] = useState("")
	const [isSuccess, setIsSuccess] = useState(false)
	const dispatch = useAppDispatch()

	const [changePassword, {data, error}] = useChangePasswordMutation()

	useEffect(() => {
		const errorMsg = (error as SerializedError)?.message;
		if(errorMsg !== undefined) {
			setIsSuccess(false)
			setLocalError(errorMsg);
		}

	}, [error, ]);

	useEffect(() => {
		if(data) {
			setLocalError("")
			setIsSuccess(true)
			formik.resetForm()
		}
	}, [data])

	const validateSchema = Yup.object().shape({
		oldPassword: Yup.string()
			.required("Это поле обязательно"),
		password: Yup.string()
			.required("Это поле обязательно")
			.min(6, "Мин длина пароля - 6"),
		confirmPassword: Yup.string().required("Это поле обязательно").test('password-match', 'Пароли не совпадают', function(value) {
			return value === this.parent.password;
		}),
	});

	const formik = useFormik({
		initialValues: {
			oldPassword: "",
			password: "",
			confirmPassword: "",
		},
		validationSchema: validateSchema,
		validateOnChange: false,
		onSubmit: (values, { resetForm }) => {
			changePassword({
				previousPassword: values.oldPassword,
				currentPassword: values.confirmPassword,
			});
		},
	});

	return (
		<form style={{display: "flex", flexDirection: "column", padding: "1rem 2rem 0"}} onSubmit={formik.handleSubmit}>
			{
				localError.length === 0 && isSuccess &&
				<Typography mb={2} color={'greem'}>Пароль успешно изменен</Typography>
			}
			{
				localError.length !== 0 &&
				<Typography mb={2} color={'error'}>{localError}</Typography>
			}


			<Stack sx={{maxWidth: '50%'}} gap={2} direction={'column'}>
				<InputTextField
					label="Текущий пароль"
					name="oldPassword"
					value={formik.values.oldPassword}
					onChange={formik.handleChange}
				/>
				<InputTextField
					label="Новый пароль"
					name="password"
					value={formik.values.password}
					onChange={formik.handleChange}
				/>
				<InputTextField
					label="Подтвердите новый пароль"
					name="confirmPassword"
					value={formik.values.confirmPassword}
					onChange={formik.handleChange}
				/>
			</Stack>
			<Stack mt={1} mb={3} justifyContent={'center'} gap={2} direction={'row'}>
				<Button variant="contained"
						type={"reset"}
						onClick={formik.handleReset}
						sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
							padding: '0 3rem',}}
						color={'error'}>
					Отмена
				</Button>
				<Button variant="contained"
						type={"submit"}
						sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
							padding: '0 3rem',}}
						color={'success'}>
					Сохранить
				</Button>
			</Stack>
		</form>
	);
};

export default ProfilePassword;