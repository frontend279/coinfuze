import React, {FC} from 'react';
import {
	Box, Divider,
	IconButton,
	MenuItem,
	Select,
	SelectChangeEvent,
	Stack,
	Typography
} from "@mui/material";
import Navbar from "../../components/Navbar/Navbar";
import LeftMenu from "../../components/LeftMenu/LeftMenu";
import ArrowCircleLeftIcon from '@mui/icons-material/ArrowCircleLeft';
import ArrowCircleRightIcon from '@mui/icons-material/ArrowCircleRight';
import {AdMaterialsBox, BottomStack, ContentBox, NewsBox, ShareAndEarnBox, TopBarBox, TopBarNavBox, TopBarSortBox} from "./styles";
import NewsCard from "../../components/NewsCard/NewsCard";
import PageHead from "../../components/PageHead/PageHead";

const News: FC = () => {
	const [newsSortVariant, setNewsSortVariant] = React.useState('10');

	const handleChange = (event: SelectChangeEvent) => {
		setNewsSortVariant(event.target.value as string);
	};


	return (
		<NewsBox flex={1}>
			<Stack direction="column">
				<PageHead  title={"Новости"}/>
				<ContentBox>
					<NewsCard/>
					<BottomStack direction="row">
						<AdMaterialsBox>
							<Typography variant={'h5'}>Рекламные материалы</Typography>
							<Divider color={'black'}/>
							<Typography>
								Делитесь с друзьями нашими рекламными материалами и зарабатывайте!

								Мы сделали для вас подборку рекламный материалов, которые вы можете использовать для
								привлечения новых участников
							</Typography>
						</AdMaterialsBox>
						<ShareAndEarnBox>
							<Typography variant={'h5'}>Рекламные материалы</Typography>
							<Divider color={'black'}/>
							<Typography>
								Делитесь с друзьями нашими рекламными материалами и зарабатывайте!

								Мы сделали для вас подборку рекламный материалов, которые вы можете использовать для
								привлечения новых участников
							</Typography>
						</ShareAndEarnBox>
					</BottomStack>
				</ContentBox>
			</Stack>
		</NewsBox>
	);
};

export default News;