
import {Box, Stack, styled} from "@mui/material";

export const NewsBox = styled(Box)(({theme}) => ({
	background: theme.palette.background.default,
	color: theme.palette.text.primary,
	padding: '0.5rem 1rem',
}))

export const TopBarBox = styled(Box)(({theme}) => ({
	display: 'flex',
	flexDirection: 'row',
	alignItems: 'center',
	justifyContent: 'space-between',
}))


export const TopBarNavBox = styled(Box)(({theme}) => ({
	display: 'flex',
	flexDirection: 'row',
	alignItems: 'center',
	gap: '1rem',
}))

export const TopBarSortBox = styled(Box)(({theme}) => ({
	display: 'flex',
	flexDirection: 'row',
	alignItems: 'center',
	gap: '1rem',
}))

export const ContentBox = styled(Box)(({theme}) => ({
	marginTop: "1rem",
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'start',
	justifyContent: 'space-between',
}))

export const AdMaterialsBox = styled(Box)(({theme}) => ({
	display: 'flex',
	flexDirection: 'column',
	backgroundColor: 'rgb(192,174,250)',
	padding: '0.7rem 2rem',
	gap: '0.4rem',
	minHeight: '20rem',
	color: 'black'
}))

export const ShareAndEarnBox = styled(Box)(({theme}) => ({
	display: 'flex',
	flexDirection: 'column',
	backgroundColor: 'rgb(176,242,206)',
	padding: '0.7rem 2rem',
	gap: '0.4rem',
	minHeight: '20rem',
	color: 'black'
}))

export const BottomStack = styled(Stack)(({theme}) => ({
	gap: '1rem',
	marginTop: '2rem'
}))