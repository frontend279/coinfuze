import React from 'react';
import {
	Stack,
} from "@mui/material";
import PageHead from "../../components/PageHead/PageHead";
import PlanManageBonus from "../../components/PlanManageBonus/PlanManageBonus";
import PlanManageInfo from "../../components/PlanManageInfo/PlanManageInfo";
import PMYoursPlans from "../../components/PlanManageYoursPlans/PMYoursPlans";
import PMAvailable from "../../components/PlanManageAvailable/PMAvailable";
import PMTable from "../../components/PlanManageTable/PMTable";

const PlanManage = () => {
	return (
		<Stack flex={1} direction={'column'} p={2} pr={4}>
			<PageHead title={'Управление планами'}/>
			<Stack mt={2} mb={2} direction={'row'} justifyContent={'space-between'} gap={1}>
				<PlanManageBonus />
				<PlanManageInfo />
			</Stack>
			<PMYoursPlans />
			<PMAvailable />
			<PMTable />
		</Stack>
	);
};

export default PlanManage;