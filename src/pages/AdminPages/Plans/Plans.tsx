import React from 'react';
import PageHead from "../../../components/PageHead/PageHead";
import UserList from "../../../components/AdminComponents/UserList/UserList";
import {Stack} from "@mui/material";
import PlanCreate from "../../../components/AdminComponents/PlanCreate/PlanCreate";
import PlansList from "../../../components/AdminComponents/PlansList/PlansList";

const Plans = () => {
    return (
        <Stack gap={2} flex={1} direction={'column'} p={2} pr={4}>
            <PageHead title={'Планы'} />
            <PlanCreate />
            <PlansList />
        </Stack>
    );
};

export default Plans;