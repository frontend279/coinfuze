import React from 'react';
import NewsCreate from "../../../components/AdminComponents/NewsCreate/NewsCreate";
import {Stack} from "@mui/material";
import PageHead from "../../../components/PageHead/PageHead";

const ANews = () => {
    return (
        <Stack direction={"column"} p={2}>
            <PageHead title={"Новости"} />
            <NewsCreate />
        </Stack>
    );
};

export default ANews;