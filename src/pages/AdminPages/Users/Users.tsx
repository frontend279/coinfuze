import React from 'react';
import PageHead from "../../../components/PageHead/PageHead";
import {Stack} from "@mui/material";
import UserList from "../../../components/AdminComponents/UserList/UserList";

const Users = () => {
    return (
        <Stack gap={2} flex={1} direction={'column'} p={2} pr={4}>
            <PageHead title={'Пользователи'} />
            <UserList/>
        </Stack>
    );
};

export default Users;