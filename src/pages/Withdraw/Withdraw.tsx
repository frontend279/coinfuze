import React, {FC} from 'react';
import {
	Stack,
} from "@mui/material";
import PageHead from "../../components/PageHead/PageHead";
import WithdrawNow from "../../components/WithdrawNow/WithdrawNow";
import WithdrawReinvest from "../../components/WithdrawReinvest/WithdrawReinvest";
import WithdrawTable from "../../components/WithdrawTable/WithdrawTable";

const Withdraw: FC = () => {
	return (
		<Stack flex={1} direction={'column'} p={2} pr={4}>
			<PageHead title={'Вывод средств'} />
			<Stack mt={2} direction={'row'} justifyContent={'space-between'} gap={1}>
				<WithdrawNow />
				<WithdrawReinvest />
			</Stack>
			<WithdrawTable />
		</Stack>
		
	);
};

export default Withdraw;