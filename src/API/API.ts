import {BaseQueryFn, createApi, FetchArgs, fetchBaseQuery, FetchBaseQueryError} from "@reduxjs/toolkit/query/react";
import {
    IAccountInfo,
    IAdminAccount,
    IAdminAccounts,
    IAuthPayload,
    IAuthTokens,
    IUpdateAccountInfo
} from "../interfaces/IAccountInfo";
import {ICreateAccount, IValidateRegisterData, IValidateRegisterPayload} from "../interfaces/ICreateAccount";
import {Mutex} from "async-mutex";
import {IChangePassword, ILoginAccount} from "../interfaces/ILoginAccount";
import {ISession} from "../interfaces/ISession";
import {IId} from "../interfaces/IId";
import {getTokens, decodeToken, setTokens} from "../Utils/Tokens";
import {
    IActiveInvesting,
    IActivePlanCreate, IActivePlanEdit, IActivePlanInvest,
    IActivePlans, IAdminPlan,
    IAdminPlans,
    IAvailablePlan
} from "../interfaces/IActivePlans";

export const baseURL = "http://localhost:2000"

interface IError  {
    data: {
        message: string,
        stack: string
    },
    status: number,
}

const baseQuery =  fetchBaseQuery({
    baseUrl: baseURL,
})

const customQuery: BaseQueryFn<
    string | FetchArgs,
    unknown,
    FetchBaseQueryError
> = async (args, api, extraOptions) => {
    let result = await baseQuery(args, api, extraOptions);


    if (result.error?.data) {
        return Promise.reject((result.error as IError).data.message);
    }
    return result;
};

export const api = createApi({
    reducerPath: 'api',
    baseQuery: customQuery,
    refetchOnMountOrArgChange: true,
    endpoints: (builder) => ({
        register: builder.mutation<IAuthPayload, ICreateAccount>({
            query: data => ({
                url: "/auth/createUser",
                method: "POST",
                body: data
            })
        }),
        validateRegister: builder.mutation<IValidateRegisterPayload, IValidateRegisterData>({
            query: data => ({
                url: "/auth/validateCreatingUser",
                method: "POST",
                body: data,
            })
        }),
        login: builder.mutation<IAuthPayload, ILoginAccount>({
            query: data => ({
                url: "/auth/login",
                method: "POST",
                body: data,
            }),
        })
    })
})


export const { useValidateRegisterMutation,
    useLoginMutation,
    useRegisterMutation } = api;


const authBaseQuery =  fetchBaseQuery({
    baseUrl: baseURL,
    prepareHeaders: (headers) => {
        // this method should retrieve the token without a hook
        const token = getTokens().access_token;

        if (token) {
            headers.set("authorization", `Bearer ${token}`);
        }
        return headers;
    },
})

const mutex = new Mutex();

const authFetchBase: BaseQueryFn<
    string | FetchArgs,
    unknown,
    FetchBaseQueryError
> = async (args, api, extraOptions) => {
    await mutex.waitForUnlock();
    let result = await authBaseQuery(args, api, extraOptions);
    if (result.error?.status === 401) {
        if (!mutex.isLocked()) {
            const release = await mutex.acquire();

            try {
                const refreshResult = await baseQuery(
                    { url: '/auth/refresh', headers: {Authorization: `Bearer ${getTokens().refresh_token}`}, },
                    api,
                    extraOptions
                );

                const data = refreshResult.data as IAuthTokens
                if (data) {
                    setTokens(data.access_token, data.refresh_token);
                    result = await authBaseQuery(args, api, extraOptions);
                }
            } finally {
                // release must be called once the mutex should be released again.
                release();
            }
        } else {
            // wait until the mutex is available without locking it
            await mutex.waitForUnlock();
            result = await authBaseQuery(args, api, extraOptions);
        }
    } else {
        if (result.error?.data) {
            return Promise.reject((result.error as IError).data.message);
        }
    }

    return result;
};

export const protectedAPI = createApi({
    reducerPath: 'protectedAPI',
    baseQuery: authFetchBase,
    refetchOnMountOrArgChange: true,
    tagTypes: ["Plan"],
    endpoints: (builder) => ({
        check: builder.mutation<IAccountInfo, void>({
            query: data => ({
                url: "/auth/check",
                method: "POST",
            })
        }),
        updateUserData: builder.mutation<IAccountInfo, FormData | IUpdateAccountInfo>({
            query: data => ({
                url: "/users/updateUserInfo",
                method: "PATCH",
                body: data,
                formData: true,
            })
        }),
        logout: builder.mutation<void, void>({
            query: data => ({
                url: "/auth/logout",
                method: "POST",
            })
        }),
        getSessions: builder.query<ISession[], void>({
            query: data => ({
                url: "/auth/sessions",
                method: "GET",
            })
        }),
        removeSession: builder.mutation<ISession[], IId>({
            query: data => ({
                url: "/auth/sessions",
                method: "DELETE",
                body: data
            })
        }),
        removeOtherSessions: builder.mutation<ISession[], void>({
            query: data => ({
                url: "/auth/allSessions",
                method: "DELETE",
            })
        }),
        changePassword: builder.mutation<void, IChangePassword>({
            query: data => ({
                url: "/auth/changePassword",
                method: "POST",
                body: data,
            })
        }),
        adminGetUsers: builder.query<IAdminAccounts[], void>({
            query: data => ({
                url: "/admin/users",
                method: "GET",
            })
        }),
        adminGetUser: builder.mutation<IAdminAccount, IId>({
            query: data => ({
                url: `/admin/user/${data.id}`,
                method: "GET",
            })
        }),
        createPlan: builder.mutation<void, IActivePlanCreate>({
            query: data => ({
                url: `/admin/plans/create`,
                method: "POST",
                body: data,
            }),
            invalidatesTags: ['Plan'],
        }),
        findAllAvailable: builder.query<IAvailablePlan[], void>({
            query: data => ({
                url: `/plans/available`,
                method: "GET",
            }),
            providesTags: ["Plan"],
        }),
        findUserPlans: builder.query<IActiveInvesting[], void>({
            query: data => ({
                url: `/plans/findUserPlans`,
                method: "GET",
            }),
            providesTags: ["Plan"],
        }),
        pickPlan: builder.mutation<IActivePlans, IId>({
            query: data => ({
                url: `/plans/pick`,
                method: "POST",
                body: data,
            }),
            invalidatesTags: ['Plan'],
        }),
        adminGetPlans: builder.query<IAdminPlans[], void>({
            query: data => ({
                url: `/admin/plans`,
                method: "GET",
            }),
            providesTags: ['Plan'],
        }),
        adminGetCurrentPlan: builder.mutation<IAdminPlan, number>({
            query: id => ({
                url: `/admin/plans/${id}`,
                method: "GET",
            }),
        }),
        adminEditCurrentPlan: builder.mutation<IAdminPlan, IActivePlanEdit>({
            query: data => ({
                url: `/admin/plans/${data.id}`,
                method: "PATCH",
                body: data,
            }),
            invalidatesTags: ['Plan'],
        }),
        investOne: builder.mutation<number, IActivePlanInvest>({
            query: data => ({
                url: `/plans/investOne`,
                method: "POST",
                body: data,
            }),
            invalidatesTags: ['Plan'],
        }),
    })
})



export const {
    useCheckMutation,
    useLogoutMutation,
    useUpdateUserDataMutation,
    useChangePasswordMutation,
    useRemoveSessionMutation,
    useGetSessionsQuery,
    useRemoveOtherSessionsMutation,
    useAdminGetUsersQuery,
    useAdminGetUserMutation,
    useCreatePlanMutation,
    useFindAllAvailableQuery,
    useFindUserPlansQuery,
    usePickPlanMutation,
    useAdminGetPlansQuery,
    useAdminGetCurrentPlanMutation,
    useAdminEditCurrentPlanMutation,
    useInvestOneMutation,
} = protectedAPI;

