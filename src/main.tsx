import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import {ThemeProvider} from "@mui/material";
import {theme} from "./theme";
import {Provider} from "react-redux";
import {store} from "./redux";

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
		<ThemeProvider theme={theme}>
			<Provider store={store}>
				<App/>
			</Provider>
		</ThemeProvider>
)
