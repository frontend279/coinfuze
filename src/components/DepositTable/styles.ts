import {
    nativeSelectClasses,
    Stack,
    styled,
    TableCell,
    tableCellClasses,
    TablePagination,
    tablePaginationClasses
} from "@mui/material";

export const HistoryStack = styled(Stack)({
    backgroundColor: '#9ad8f0',
    color: 'black',
})


export const HistoryTableCell = styled(TableCell)(({theme}) => ({
    [`&.${tableCellClasses.head}`]: {
        color: theme.palette.common.black,
    },
    [`&.${tableCellClasses.body}`]: {
        color: theme.palette.common.black,
    },
}));

export const HistoryTablePagination = styled(TablePagination)(({theme}) => ({
    [`&.${tablePaginationClasses.root}`]: {
        color: theme.palette.common.black,
    },
    [`&.${nativeSelectClasses.icon}`]: {
        color: theme.palette.common.black,
    },
    '& .MuiSvgIcon-root': {
        color: theme.palette.common.black,
    },
}))