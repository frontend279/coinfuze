import React, {useState} from 'react';
import {Divider, Table, TableBody, TableHead, TableRow, Typography} from "@mui/material";
import {HistoryStack, HistoryTableCell, HistoryTablePagination} from "./styles";


interface IHistoryRows {
    date: string;
    currency: string;
    summary: string;
    comment: string;
    status: string;
}

const DepositTable = () => {

    const [historyRows, setHistoryRows] = useState<IHistoryRows[]>([])
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);


    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <HistoryStack p={2} direction={'column'} mt={3}>
            <Typography mb={3} variant={'h5'}>История</Typography>
            <Divider color={'black'}/>
            <Table sx={{minWidth: 650}}>
                <TableHead>
                    <TableRow>
                        <HistoryTableCell align="center">Дата</HistoryTableCell>
                        <HistoryTableCell align="center">Валюта вывода</HistoryTableCell>
                        <HistoryTableCell align="center">Сумма вывода</HistoryTableCell>
                        <HistoryTableCell align="center">Комментарий</HistoryTableCell>
                        <HistoryTableCell align="center">Статус транзакции</HistoryTableCell>
                    </TableRow>
                </TableHead>
                {historyRows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(historyRow => (
                    <TableBody>
                        <HistoryTableCell align="center">{historyRow.date}</HistoryTableCell>
                        <HistoryTableCell align="center">{historyRow.currency}</HistoryTableCell>
                        <HistoryTableCell align="center">{historyRow.summary}</HistoryTableCell>
                        <HistoryTableCell align="center">{historyRow.comment}</HistoryTableCell>
                        <HistoryTableCell align="center">{historyRow.status}</HistoryTableCell>
                    </TableBody>
                ))
                }
                {
                    !historyRows.length && (
                        <TableBody>
                            <HistoryTableCell colSpan={5} align="center">Транзакций не обнаружено</HistoryTableCell>
                        </TableBody>
                    )
                }
            </Table>
            <HistoryTablePagination
                sx={{color: 'white'}}
                rowsPerPageOptions={[5, 10, 15]}
                count={historyRows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                labelRowsPerPage={'Кол-во строк:'}
            />
        </HistoryStack>
    );
};

export default DepositTable;