import React, {useEffect, useState} from 'react';
import {Stack} from "@mui/material";
import PlanItem from "../PlanItem/PlanItem";
import {useAdminGetPlansQuery} from "../../../API/API";
import {IAdminPlans} from "../../../interfaces/IActivePlans";

const PlansList = () => {

    const {data} = useAdminGetPlansQuery();

    const [adminPlans, setAdminPlans] = useState<IAdminPlans[]>([] as IAdminPlans[]);

    useEffect(() => {
        if(data) {
            setAdminPlans([...data].sort((a, b) => a.id - b.id));
        }
    }, [data, ]);

    return (
        <Stack>
            {
                adminPlans?.length && adminPlans.map(item => (
                    <PlanItem key={item.id} id={item.id} />
                ))
            }
        </Stack>
    );
};

export default PlansList;