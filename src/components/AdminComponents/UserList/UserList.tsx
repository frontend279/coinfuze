import React from 'react';
import {Stack} from "@mui/material";
import UserItem from "../UserItem/UserItem";
import {useAdminGetUsersQuery} from "../../../API/API";

const UserList = () => {

    const {data} = useAdminGetUsersQuery();

    return (
        <Stack gap={3} alignItems={''} direction={"column"}>
            {
                data && data.map(current => (
                    <UserItem key={current.id} userInfo={current} />
                ))
            }
        </Stack>
    );
};

export default UserList;