import React, {FC} from 'react';
import {Button, Stack, Table, TableBody, TableHead, TableRow} from "@mui/material";
import {TransactioTableCell, TransactioTablePagination} from "../UserItem/styles";
import {HistoryTableCell} from "../../PlanManageTable/styles";
import {InvestorTableCell, InvestorTablePagination} from "./styles";
import {IAdminPlan} from "../../../interfaces/IActivePlans";
import {formatTimeFromDatabase, formatTimeString} from "../../../Utils/Utils";

interface IPlanInvestors {
    plan: IAdminPlan;
}

const PlanInvestors: FC<IPlanInvestors> = ({plan}) => {
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <div>
            <Table sx={{ minWidth: 650, minHeight: 400 }}>
                <TableHead >
                    <TableRow >
                        <InvestorTableCell align="center">Дата</InvestorTableCell>
                        <InvestorTableCell align="center">Пользователь</InvestorTableCell>
                        <InvestorTableCell align="center">Сумма</InvestorTableCell>
                        <InvestorTableCell align="center">Действия</InvestorTableCell>
                    </TableRow>
                </TableHead>
                { plan.Investings?.length && plan.Investings.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(investing => (
                        <TableBody>
                            <InvestorTableCell align="center">{formatTimeFromDatabase(investing.createdAt).date}
                                <br />
                                {formatTimeFromDatabase(investing.createdAt).time}
                            </InvestorTableCell>
                            <InvestorTableCell align="center">{investing.user.login}</InvestorTableCell>
                            <InvestorTableCell align="center">{investing.money ? investing.money : "Ожидание"}</InvestorTableCell>
                            <InvestorTableCell align="center">
                                <Stack direction={'row'} gap={1} justifyContent={'center'} alignItems={'center'}>
                                    <Button variant="contained"
                                            type={'submit'}
                                            sx={{borderRadius: '20px', maxWidth: '100px', minHeight: '40px',
                                                fontSize: "10px",
                                                padding: '0 3rem',}}
                                            color={'error'}>
                                        Удалить
                                    </Button>
                                    <Button variant="contained"
                                            type={'submit'}
                                            sx={{borderRadius: '20px', maxWidth: '100px', minHeight: '40px',
                                                fontSize: "10px",
                                                padding: '0 3rem',}}
                                            color={'warning'}>
                                        Редактировать
                                    </Button>
                                </Stack>
                            </InvestorTableCell>
                        </TableBody>
                    ))
                }
                {
                    !plan.Investings?.length && (
                        <TableBody>
                            <InvestorTableCell colSpan={4} align="center">Транзакций не обнаружено</InvestorTableCell>
                        </TableBody>
                    )
                }
            </Table>
            <InvestorTablePagination
                sx={{color: 'white'}}
                rowsPerPageOptions={[5]}
                count={5}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                labelRowsPerPage={'Кол-во строк:'}
            />
        </div>
    );
};

export default PlanInvestors;