import {Avatar, Button, Collapse, Stack, Tab, Tabs, Typography} from '@mui/material';
import React, {FC, useEffect, useState} from 'react';
import UserPersonalData from "../UserItem/UserPersonalData";
import UserTransactions from "../UserItem/UserTransactions";
import PlanEdit from "./PlanEdit";
import PlanInfo from "./PlanInfo";
import {IconWrapper, PlansInfoStack, PlansItemStack} from "./styles";
import PlanInvestors from "./PlanInvestors";
import {IAdminPlan, IAdminPlans} from "../../../interfaces/IActivePlans";
import {useAdminGetCurrentPlanMutation, useAdminGetPlansQuery} from "../../../API/API";


const PlanItem: FC<IAdminPlans> = (plan) => {

    const [isFullOpen, setFullOpen] = useState(false);

    const [tab, setTab] = useState(0);

    const [fetchCurrentPlan, {data}] = useAdminGetCurrentPlanMutation();

    useEffect(() => {
        if(isFullOpen) {
            fetchCurrentPlan(plan.id)
        }
    }, [isFullOpen, ]);

    const handleOpen = () => {
        setFullOpen(!isFullOpen);
    }


    const getTab = () => {
        switch (tab) {
            case 0:
                return <PlanInfo plan={data as IAdminPlan} />
            case 1:
                return <PlanEdit plan={data as IAdminPlan} />
            case 2:
                return <PlanInvestors plan={data as IAdminPlan} />
        }
    }

    return (
        <Stack direction={'column'}>
            <PlansItemStack isOpen={isFullOpen} justifyContent={'space-between'} alignItems={'center'} color={'white'} direction={'row'}>
                <Stack gap={2} alignItems={'center'}  direction={'row'}>
                    <IconWrapper>
                        #{plan.id}
                    </IconWrapper>
                    <Typography variant={'h6'}>
                        {"userInfo.login"}
                    </Typography>
                </Stack>
                <Stack>
                    <Button variant={'contained'}
                            color={"secondary"}
                            onClick={handleOpen}>{isFullOpen ? "Свернуть" : "Подробнее..."}</Button>
                </Stack>
            </PlansItemStack>
            <Collapse in={isFullOpen}>
                <PlansInfoStack>
                    <Tabs value={tab}>
                        <Tab onClick={() => setTab(0)} label="Информация"/>
                        <Tab onClick={() => setTab(1)} label="Редактировать"/>
                        <Tab onClick={() => setTab(2)} label="Инвесторы"/>
                    </Tabs>
                    {
                       data && getTab()
                    }
                </PlansInfoStack>
            </Collapse>
        </Stack>
    );
};

export default PlanItem;