import React, {FC} from 'react';
import {Stack, Typography} from "@mui/material";
import {PlayInfoStack} from "./styles";
import {IAdminPlan} from "../../../interfaces/IActivePlans";
import {formatTimeFromDatabase, formatTimeString, getTimeDiff} from "../../../Utils/Utils";

interface IPlanInfo {
    plan: IAdminPlan;
}

const PlanInfo: FC<IPlanInfo> = ({plan}) => {
    return (
        <Stack gap={4} direction={'row'}>
            <PlayInfoStack flex={1}>
                <Typography variant={'h6'}>Дата начала: {formatTimeString(plan.startDate).date}</Typography>
                <Typography variant={'h6'}>Дата окончания: {formatTimeString(plan.endDate).date}</Typography>
                <Typography variant={'h6'}>Мин. инвестиции: {plan.minInvest}</Typography>
                <Typography variant={'h6'}>Срок: {getTimeDiff(+plan.startDate, +plan.endDate).days}</Typography>
                <Typography variant={'h6'}>Предпол. доходность: {plan.waitingFinanceResult}</Typography>
            </PlayInfoStack>
            <PlayInfoStack flex={1}>
                <Typography variant={'h6'}>Финансовый результат: {plan.financeResult}</Typography>
                <Typography variant={'h6'}>Статус плана: {plan.status}</Typography>
            </PlayInfoStack>
        </Stack>
    );
};

export default PlanInfo;