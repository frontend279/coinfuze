import {
    nativeSelectClasses,
    Stack,
    styled,
    TableCell,
    tableCellClasses,
    TablePagination,
    tablePaginationClasses
} from "@mui/material";

interface IUserItemStack {
    isOpen: boolean
}

export const PlansItemStack = styled(Stack, {
    shouldForwardProp: (prop) => prop !== "isOpen",
})<IUserItemStack>(({isOpen} ) =>  ({
    backgroundColor: "#121212",
    border: '2px solid #0e0e0e',
    padding: "1rem",
    borderRadius: isOpen ? 0 : "15px",
    borderTopLeftRadius: isOpen ? "15px" : 0,
    borderTopRightRadius: isOpen ? "15px" : 0,
    boxShadow: "rgba(0, 0, 0, 0.2) 0px 11px 15px -7px, rgba(0, 0, 0, 0.14) 0px 24px 38px 3px, rgba(0, 0, 0, 0.12) 0px 9px 46px 8px",
    ":hover": {
        transform: isOpen ? "" : "scale(1.01)",
        transition: "0.5s",
    }
}))

export const PlansInfoStack = styled(Stack)({
    backgroundColor: "#151515",
    borderBottomLeftRadius: "15px",
    borderBottomRightRadius: "15px",
})

export const IconWrapper = styled(Stack)({
    width: "50px",
    height: "50px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    fontSize: "24px"
})

export const PlayInfoStack = styled(Stack)({
    color: "white",
    padding: "1rem",
})

export const EditPlansStack = styled(Stack)(({theme} ) =>  ({
    backgroundColor: "#121212",
    border: '2px solid #0e0e0e',
    padding: "1rem",
    boxShadow: "rgba(0, 0, 0, 0.2) 0px 11px 15px -7px, rgba(0, 0, 0, 0.14) 0px 24px 38px 3px, rgba(0, 0, 0, 0.12) 0px 9px 46px 8px",
}))

export const InvestorTableCell = styled(TableCell)(({theme}) => ({
    [`&.${tableCellClasses.head}`]: {
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        color: theme.palette.common.white,
    },
}));

export const InvestorTablePagination = styled(TablePagination)(({theme}) => ({
    [`&.${tablePaginationClasses.root}`]: {
        color: theme.palette.common.white,
    },
    [`&.${nativeSelectClasses.icon}`]: {
        color: theme.palette.common.white,
    },

    '& .MuiSvgIcon-root': {
        color: theme.palette.common.white,
    },
}))