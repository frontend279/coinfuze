import React, {FC} from 'react';
import {Button, Stack, TextField, Typography} from "@mui/material";
import * as Yup from "yup";
import {useFormik} from "formik";
import {CreatePlansStack} from "../PlanCreate/styles";
import {DateField, LocalizationProvider} from "@mui/x-date-pickers";
import {AdapterDateFns} from "@mui/x-date-pickers/AdapterDateFnsV3";
import {EditPlansStack} from "./styles";
import {IAdminPlan} from "../../../interfaces/IActivePlans";
import {useAdminEditCurrentPlanMutation} from "../../../API/API";

interface IPlanEdit {
    plan: IAdminPlan,
}

const PlanEdit: FC<IPlanEdit> = ({plan}) => {

    const [AdminEdit, {data}] = useAdminEditCurrentPlanMutation();

    // const validateSchema = Yup.object().shape({
    //     minInvest: Yup.number().required("Это поле обязательно"),
    //     waitingFinanceResult: Yup.number().required("Это поле обязательно"),
    //     startDate: Yup.date().required("Это поле обязательно"),
    //     endDate: Yup.date().required("Это поле обязательно"),
    //     startFund: Yup.number().required("Это поле обязательно"),
    //     addedFund: Yup.number().required("Это поле обязательно"),
    //     financeResult: Yup.number().required("Это поле обязательно"),
    //     investedMoney: Yup.number().required("Это поле обязательно"),
    //     profit: Yup.number().required("Это поле обязательно"),
    //     planCurrency: Yup.string().required("Это поле обязательно"),
    // });

    const getDateTime = (time: number) => {
        const date = new Date();
        date.setTime(time);
        return date.toDateString();
    }



    const formik = useFormik({
        initialValues: {
            minInvest: plan.minInvest,
            waitingFinanceResult: plan.waitingFinanceResult,
            startDate: plan.startDate,
            endDate: plan.endDate,
            planCurrency: plan.planCurrency,
            financeResult: plan.financeResult,
            investedMoney: plan.investedMoney,
        },
        //validationSchema: validateSchema,
        validateOnChange: false,
        validateOnMount: false,
        validateOnBlur: false,
        onSubmit: (values, { resetForm }) => {
            AdminEdit({
                ...values,
                id: plan.id,
            })
        },
    });

    const handleRemove = () => {

    }

    return (
        <form onSubmit={formik.handleSubmit}>
            <EditPlansStack gap={3} alignItems={''} direction={"row"}>
                <Stack gap={2}>
                    <TextField value={formik.values.minInvest}
                               autoComplete='off'
                               onChange={formik.handleChange}
                               name={"minInvest"}
                               type={'number'}
                               label="мин. вклад"
                               variant="outlined"
                               error={!!formik.errors.minInvest?.length}
                               helperText={formik.errors.minInvest}/>
                    <TextField value={formik.values.waitingFinanceResult}
                               autoComplete='off'
                               onChange={formik.handleChange}
                               type={'number'}
                               name={"waitingFinanceResult"}
                               label="ожидаемый доход, %"
                               variant="outlined"
                               error={!!formik.errors.waitingFinanceResult?.length}
                               helperText={formik.errors.waitingFinanceResult}/>
                    <TextField value={formik.values.planCurrency}
                               autoComplete='off'
                               onChange={formik.handleChange}
                               name={"planCurrency"}
                               label="валюта контракта"
                               variant="outlined"
                               error={!!formik.errors.planCurrency?.length}
                               helperText={formik.errors.planCurrency}/>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <DateField
                            label="Начало плана"
                            format="dd/MM/yyyy"
                            name={"startDate"}
                            value={getDateTime(+formik.values.startDate)}
                            onChange={(value) => formik.setFieldValue("startDate", value)}
                            slotProps={{
                                textField: {
                                    variant: "outlined",
                                    error: !!formik.errors.startDate,
                                    helperText: formik.errors.startDate
                                }
                            }}
                        />
                    </LocalizationProvider>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <DateField
                            label="Окончание плана"
                            format="dd/MM/yyyy"
                            name={"endDate"}
                            value={getDateTime(+formik.values.endDate)}
                            onChange={(value) => formik.setFieldValue("endDate", value)}
                            slotProps={{
                                textField: {
                                    variant: "outlined",
                                    error: !!formik.errors.endDate,
                                    helperText: formik.errors.endDate
                                }
                            }}
                        />
                    </LocalizationProvider>
                </Stack>
                <Stack gap={2}>
                    <TextField value={formik.values.financeResult}
                               autoComplete='off'
                               onChange={formik.handleChange}
                               name={"financeResult"}
                               type={'number'}
                               label="Финансовый результат"
                               variant="outlined"
                               error={!!formik.errors.planCurrency?.length}
                               helperText={formik.errors.planCurrency}/>
                </Stack>
                <Stack gap={2}>
                    <Button variant="contained"
                            type={"reset"}
                            onClick={handleRemove}
                            sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
                                padding: '0 3rem',}}
                            color={'error'}>
                        Удалить
                    </Button>
                    <Button variant="contained"
                            type={"reset"}
                            onClick={formik.handleReset}
                            sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
                                padding: '0 3rem',}}
                            color={'warning'}>
                        Отмена
                    </Button>
                    <Button variant="contained"
                            type={'submit'}
                            sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
                                padding: '0 3rem',}}
                            color={'success'}>
                        Сохранить
                    </Button>
                </Stack>
            </EditPlansStack>
        </form>
    );
};

export default PlanEdit;