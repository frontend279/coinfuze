import React, {FC} from 'react';
import {Button, Stack, Typography} from "@mui/material";
import {PersonalDataStack} from "./styles";
import {IAdminAccount, IAdminAccounts} from "../../../interfaces/IAccountInfo";

const UserPersonalData: FC<{data: IAdminAccount}> = ({data}) => {
    return (
        <>
            <PersonalDataStack justifyContent={'space-around'} direction={'row'}>
                <Stack gap={1}>
                    <Typography variant={'h6'}>Фамилия: {data.userInfo.lastName}</Typography>
                    <Typography variant={'h6'}>Имя: {data.userInfo.firstName}</Typography>
                    <Typography variant={'h6'}>Отчество: {data.userInfo.middleName}</Typography>
                    <Typography variant={'h6'}>Дата рождения: {data.userInfo.birthday}</Typography>
                    <Typography variant={'h6'}>Номер телефона: {data.userInfo.phone}</Typography>
                    <Typography variant={'h6'}>Логин: {data.userInfo.phone}</Typography>
                </Stack>
                <Stack gap={1}>
                    <Typography variant={'h6'}>Страна: {data.userInfo.country}</Typography>
                    <Typography variant={'h6'}>Город: {data.userInfo.city}</Typography>
                    <Typography variant={'h6'}>Адрес 1: {data.userInfo.addressFirst}</Typography>
                    <Typography variant={'h6'}>Адрес 2: {data.userInfo.addressLast}</Typography>
                    <Typography variant={'h6'}>Почтовый код: {data.userInfo.postalCode}</Typography>
                </Stack>
            </PersonalDataStack>
            <hr color={'black'}/>
            <PersonalDataStack direction={'row'}>
                <Stack gap={1}>
                    <Typography variant={'h6'}>Номер паспорта: тест</Typography>
                    <Typography variant={'h6'}>Серия: тест</Typography>
                </Stack>
            </PersonalDataStack>
            <hr color={'black'}/>
            <PersonalDataStack flexWrap={"wrap"} gap={1} direction={'row'}>
                <Typography variant={'h6'}>Действия модератора:</Typography>
                <Button variant={"contained"} color={'success'}>Верифицировать</Button>
                <Button variant={"contained"} color={'warning'}>Изменить данные</Button>
                <Button variant={"contained"} color={'error'}>Заблокировать</Button>
                <Button variant={"contained"} color={'error'}>Удалить</Button>
                <Button></Button>
            </PersonalDataStack>
        </>

    )
        ;
};

export default UserPersonalData;
