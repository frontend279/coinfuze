import React, {FC, useEffect, useState} from 'react';
import {Avatar, Button, Collapse, Stack, Tab, Tabs, Typography} from "@mui/material";
import {IconWrapper, UserInfoStack, UserItemStack} from "./styles";
import UserPersonalData from "./UserPersonalData";
import UserTransactions from "./UserTransactions";
import {IAdminAccount, IAdminAccounts} from "../../../interfaces/IAccountInfo";
import users from "../../../pages/AdminPages/Users/Users";
import {useAdminGetUserMutation, useAdminGetUsersQuery} from "../../../API/API";
import {current} from "@reduxjs/toolkit";
import {getAvatarURL} from "../../../Utils/Utils";

const UserItem: FC<{userInfo: IAdminAccounts}> = ({userInfo}) => {
   const [isFullOpen, setFullOpen] = useState(false);
   const [tab, setTab] = useState(0);

    const [GetUserData, {data}] = useAdminGetUserMutation();
    const [currentData, setCurrentData] =
        useState<IAdminAccount>({} as IAdminAccount)

    const handleOpen = () => {

        setFullOpen(!isFullOpen);
        GetUserData({id: userInfo.id})
    }

    useEffect(() => {
        if(data) {
            setCurrentData(data);
        }
    }, [data, ]);

    const getTab = () => {
        switch (tab) {
            case 0:
                return <UserPersonalData data={currentData} />
            case 1:
                return <UserTransactions data={currentData} />
        }
    }
    return (
        <Stack direction={'column'}>
            <UserItemStack isOpen={isFullOpen} justifyContent={'space-between'} alignItems={'center'} color={'white'} direction={'row'}>
                <Stack gap={2} alignItems={'center'}  direction={'row'}>
                    <IconWrapper>
                        <Avatar sx={{ width: "100%", height: "100%" }} alt="user image" src={getAvatarURL(userInfo.userInfo?.avatar)}/>
                    </IconWrapper>
                    <Typography variant={'h6'}>
                        {userInfo.login}
                    </Typography>
                </Stack>
                <Stack>
                    <Button variant={'contained'}
                            color={"secondary"}
                            onClick={handleOpen}>{isFullOpen ? "Свернуть" : "Подробнее..."}</Button>
                </Stack>
            </UserItemStack>
            <Collapse in={isFullOpen}>
                <UserInfoStack>
                    <Tabs value={tab}>
                        <Tab onClick={() => setTab(0)} label="Личные данные"/>
                        <Tab onClick={() => setTab(1)} label="Транзакции"/>
                        <Tab onClick={() => setTab(2)} label="История блокировок"/>
                    </Tabs>
                    {
                        currentData.userInfo && getTab()
                    }
                </UserInfoStack>
            </Collapse>
        </Stack>
    );
};

export default UserItem;