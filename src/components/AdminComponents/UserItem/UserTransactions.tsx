import React, {FC, useState} from 'react';
import {TransactioTableCell, TransactioTablePagination, UserTransactionsStack} from "./styles";
import {Table, TableBody, TableHead, TableRow} from "@mui/material";
import {HistoryTableCell} from "../../PlanManageTable/styles";
import {IAdminAccount} from "../../../interfaces/IAccountInfo";

const UserTransactions: FC<{data: IAdminAccount}> = ({data}) => {

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);


    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <UserTransactionsStack>
            <Table sx={{ minWidth: 650, minHeight: 400 }}>
                <TableHead >
                    <TableRow >
                        <TransactioTableCell align="center">Дата</TransactioTableCell>
                        <TransactioTableCell align="center">Статус</TransactioTableCell>
                        <TransactioTableCell align="center">Действие</TransactioTableCell>
                        <TransactioTableCell align="center">Кол-во</TransactioTableCell>
                        <TransactioTableCell align="center">Валюта</TransactioTableCell>
                        <TransactioTableCell align="center">Комментарий</TransactioTableCell>
                    </TableRow>
                </TableHead>
                {data.transactions.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(transaction => (
                        <TableBody>
                            <TransactioTableCell align="center">{transaction.createdAt}</TransactioTableCell>
                            <TransactioTableCell align="center">{transaction.status}</TransactioTableCell>
                            <TransactioTableCell align="center">{transaction.action}</TransactioTableCell>
                            <TransactioTableCell align="center">{transaction.amount}</TransactioTableCell>
                            <TransactioTableCell align="center">{transaction.currency}</TransactioTableCell>
                            <TransactioTableCell align="center">{transaction.comment}</TransactioTableCell>
                        </TableBody>
                    ))
                }
                {
                    !data.transactions.length && (
                        <TableBody>
                            <HistoryTableCell colSpan={7} align="center">Транзакций не обнаружено</HistoryTableCell>
                        </TableBody>
                    )
                }
            </Table>
            <TransactioTablePagination
                sx={{color: 'white'}}
                rowsPerPageOptions={[5]}
                count={data.transactions.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                labelRowsPerPage={'Кол-во строк:'}
            />
        </UserTransactionsStack>
    );
};

export default UserTransactions;