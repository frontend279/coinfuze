import React from 'react';
import UserItem from "../UserItem/UserItem";
import {Button, Stack, TextField} from "@mui/material";
import {useCreatePlanMutation} from "../../../API/API";
import {CreatePlansStack} from "./styles";
import {DateField, LocalizationProvider} from "@mui/x-date-pickers";
import {AdapterDateFns} from "@mui/x-date-pickers/AdapterDateFnsV3";
import * as Yup from "yup";
import {useFormik} from "formik";
import {ICreateAccount} from "../../../interfaces/ICreateAccount";
import dayjs, {Dayjs} from "dayjs";

const PlanCreate = () => {

    const [CreatePlan, {data, error} ] = useCreatePlanMutation();

    const validateSchema = Yup.object().shape({
        minInvest: Yup.number().required("Это поле обязательно"),
        waitingFinanceResult: Yup.number().required("Это поле обязательно"),
        startDate: Yup.date().required("Это поле обязательно"),
        endDate: Yup.date().required("Это поле обязательно"),
        planCurrency: Yup.string().required("Это поле обязательно"),
    });

    const formik = useFormik({
        initialValues: {
            minInvest: 0,
            waitingFinanceResult: 0,
            startDate: 0,
            endDate: 0,
            planCurrency: "",
        },
        validationSchema: validateSchema,
        validateOnChange: false,
        validateOnMount: false,
        validateOnBlur: false,
        onSubmit: (values, { resetForm }) => {
            const startDateTime = new Date(values.startDate);
            const endDateTime = new Date(values.endDate);
            CreatePlan({
                ...values,
                startDate: startDateTime.getTime().toString(),
                endDate: endDateTime.getTime().toString(),
            });
        },
    });

    return (
        <form onSubmit={formik.handleSubmit}>
            <CreatePlansStack gap={3} alignItems={''} direction={"row"}>
                <Stack gap={2}>
                    <TextField value={formik.values.minInvest}
                               autoComplete='off'
                               onChange={formik.handleChange}
                               type={'number'}
                               name={"minInvest"}
                               label="мин. вклад"
                               variant="outlined"
                               error={!!formik.errors.minInvest?.length}
                               helperText={formik.errors.minInvest}/>
                    <TextField value={formik.values.waitingFinanceResult}
                               autoComplete='off'
                               onChange={formik.handleChange}
                               name={"waitingFinanceResult"}
                               label="ожидаемый доход, %"
                               type={'number'}
                               variant="outlined"
                               error={!!formik.errors.waitingFinanceResult?.length}
                               helperText={formik.errors.waitingFinanceResult}/>
                    <TextField value={formik.values.planCurrency}
                               autoComplete='off'
                               onChange={formik.handleChange}
                               name={"planCurrency"}
                               label="валюта контракта"
                               variant="outlined"
                               error={!!formik.errors.planCurrency?.length}
                               helperText={formik.errors.planCurrency}/>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <DateField
                            label="Начало плана"
                            format="dd/MM/yyyy"
                            name={"startDate"}
                            value={formik.values.startDate}
                            onChange={(value) => formik.setFieldValue("startDate", value)}
                            slotProps={{
                                textField: {
                                    variant: "outlined",
                                    error: !!formik.errors.startDate,
                                    helperText: formik.errors.startDate
                                }
                            }}
                        />
                    </LocalizationProvider>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <DateField
                            label="Окончание плана"
                            format="dd/MM/yyyy"
                            name={"endDate"}
                            value={formik.values.endDate}
                            onChange={(value) => formik.setFieldValue("endDate", value)}
                            slotProps={{
                                textField: {
                                    variant: "outlined",
                                    error: !!formik.errors.endDate,
                                    helperText: formik.errors.endDate
                                }
                            }}
                        />
                    </LocalizationProvider>
                </Stack>
                <Stack gap={2}>
                    <Button variant="contained"
                            type={"reset"}
                            onClick={formik.handleReset}
                            sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
                                padding: '0 3rem',}}
                            color={'error'}>
                        Отмена
                    </Button>
                    <Button variant="contained"
                            type={'submit'}
                            sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
                                padding: '0 3rem',}}
                            color={'success'}>
                        Сохранить
                    </Button>
                </Stack>
            </CreatePlansStack>
        </form>
    );
};

export default PlanCreate;