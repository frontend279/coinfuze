import {Stack, styled} from "@mui/material";

export const CreateNewsStack = styled(Stack)(({theme} ) =>  ({
    backgroundColor: "#121212",
    border: '2px solid #0e0e0e',
    padding: "1rem",
    boxShadow: "rgba(0, 0, 0, 0.2) 0px 11px 15px -7px, rgba(0, 0, 0, 0.14) 0px 24px 38px 3px, rgba(0, 0, 0, 0.12) 0px 9px 46px 8px",
}))