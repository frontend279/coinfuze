import React, {useState} from 'react';
import {useCreatePlanMutation} from "../../../API/API";
import * as Yup from "yup";
import {useFormik} from "formik";
import {CreatePlansStack} from "../PlanCreate/styles";
import {Button, Stack, TextField} from "@mui/material";
import {DateField, LocalizationProvider} from "@mui/x-date-pickers";
import {AdapterDateFns} from "@mui/x-date-pickers/AdapterDateFnsV3";
import {CreateNewsStack} from "./style";

const NewsCreate = () => {
    const [CreatePlan, {data, error} ] = useCreatePlanMutation();

    const [image, setImage] = useState<File>()

    const formik = useFormik({
        initialValues: {
            title: "",
            description: "",
        },
        //validationSchema: validateSchema,
        validateOnChange: false,
        validateOnMount: false,
        validateOnBlur: false,
        onSubmit: (values, { resetForm }) => {

        },
    });

    return (
        <form onSubmit={formik.handleSubmit}>
            <CreateNewsStack gap={3} alignItems={''} direction={"row"}>
                <Stack flex={1} gap={2}>
                    <TextField value={formik.values.title}
                               autoComplete='off'
                               onChange={formik.handleChange}
                               name={"title"}
                               label="Заголовок"
                               variant="outlined"/>
                    <TextField value={formik.values.description}
                               multiline
                               autoComplete='off'
                               onChange={formik.handleChange}
                               name={"description"}
                               label="Описание"
                               variant="outlined"
                    />
                </Stack>
                <Stack gap={2}>
                    <Button variant="contained"
                            type={"reset"}
                            onClick={formik.handleReset}
                            sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
                                padding: '0 3rem',}}
                            color={'error'}>
                        Отмена
                    </Button>
                    <Button variant="contained"
                            type={'submit'}
                            sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
                                padding: '0 3rem',}}
                            color={'success'}>
                        Сохранить
                    </Button>
                    <Button variant="contained"
                            type={'submit'}
                            sx={{borderRadius: '20px', maxWidth: '300px', minHeight: '40px',
                                padding: '0 3rem',}}
                            color={'success'}>
                        Опубликовать
                    </Button>
                </Stack>
            </CreateNewsStack>
        </form>
    )
};

export default NewsCreate;