import React, {useState} from 'react';
import {Divider, Table, TableBody, TableHead, TableRow, Typography} from "@mui/material";
import {HistoryStack, HistoryTableCell, HistoryTablePagination} from "./styles";


interface IPMTable {
    dateStart: string;
    dateEnd: string;
    startSummary: string;
    addedSummary: string;
    financeResult: string;
    resultSummary: string;
    status: string;
}

const PMTable = () => {

    const [planManageRows, setPlanManageRows] = useState<IPMTable[]>([
        {
            dateStart: '17.06.23',
            dateEnd: '20.06.23',
            startSummary: '300$',
            addedSummary: '27$',
            financeResult: '60%',
            resultSummary: '600$',
            status: 'disabled',
        },
        {
            dateStart: '17.06.23',
            dateEnd: '20.06.23',
            startSummary: '300$',
            addedSummary: '27$',
            financeResult: '60%',
            resultSummary: '600$',
            status: 'disabled',
        },
        {
            dateStart: '17.06.23',
            dateEnd: '20.06.23',
            startSummary: '300$',
            addedSummary: '27$',
            financeResult: '60%',
            resultSummary: '600$',
            status: 'disabled',
        },
        {
            dateStart: '17.06.24',
            dateEnd: '20.06.23',
            startSummary: '300$',
            addedSummary: '27$',
            financeResult: '60%',
            resultSummary: '600$',
            status: 'disabled',
        },
        {
            dateStart: '17.06.24',
            dateEnd: '20.06.23',
            startSummary: '300$',
            addedSummary: '27$',
            financeResult: '60%',
            resultSummary: '600$',
            status: 'disabled',
        },{
            dateStart: '17.06.24',
            dateEnd: '20.06.23',
            startSummary: '300$',
            addedSummary: '27$',
            financeResult: '60%',
            resultSummary: '600$',
            status: 'disabled',
        },

    ])

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);


    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };


    return (
        <HistoryStack p={2} mt={2} direction={'column'}>
            <Typography mb={3} variant={'h5'}>История</Typography>
            <Divider color={'black'} />
            <Table sx={{ minWidth: 650 }}>
                <TableHead >
                    <TableRow >
                        <HistoryTableCell align="center">Дата начала</HistoryTableCell>
                        <HistoryTableCell align="center">Дата окончания</HistoryTableCell>
                        <HistoryTableCell align="center">Стартовый капитал</HistoryTableCell>
                        <HistoryTableCell align="center">Добавленный капитал</HistoryTableCell>
                        <HistoryTableCell align="center">Финансовый результат</HistoryTableCell>
                        <HistoryTableCell align="center">Прибыль в USDT</HistoryTableCell>
                        <HistoryTableCell align="center">Статус Плана</HistoryTableCell>
                    </TableRow>
                </TableHead>
                {planManageRows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(planManageRow => (
                        <TableBody>
                            <HistoryTableCell align="center">{planManageRow.dateStart}</HistoryTableCell>
                            <HistoryTableCell align="center">{planManageRow.dateEnd}</HistoryTableCell>
                            <HistoryTableCell align="center">{planManageRow.startSummary}</HistoryTableCell>
                            <HistoryTableCell align="center">{planManageRow.addedSummary}</HistoryTableCell>
                            <HistoryTableCell align="center">{planManageRow.financeResult}</HistoryTableCell>
                            <HistoryTableCell align="center">{planManageRow.resultSummary}</HistoryTableCell>
                            <HistoryTableCell align="center">{planManageRow.status}</HistoryTableCell>
                        </TableBody>
                    ))
                }
                {
                    !planManageRows.length && (
                        <TableBody>
                            <HistoryTableCell colSpan={7} align="center">Транзакций не обнаружено</HistoryTableCell>
                        </TableBody>
                    )
                }
            </Table>
            <HistoryTablePagination
                sx={{color: 'white'}}
                rowsPerPageOptions={[5, 10, 15]}
                count={planManageRows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                labelRowsPerPage={'Кол-во строк:'}
            />
        </HistoryStack>
    );
};

export default PMTable;