import React from 'react';
import {Box, Stack} from "@mui/material";
import {Typography} from "@mui/material/";

const Footbar = () => {
    return (
        <Stack mt={4} p={2} direction={'row'} color={'white'} justifyContent={'space-between'}>
            <Typography variant={'body1'}>
                &copy; 2022 CoinFuze. Все права защищены.
            </Typography>
            <Stack direction={'row'} gap={1}>
                <Typography variant={'body1'}>Условия использование</Typography>
                <Typography variant={'body1'}>|</Typography>
                <Typography variant={'body1'}>Политика конфиденциальности</Typography>
                <Typography variant={'body1'}>|</Typography>
                <Typography variant={'body1'}>Партнерская программа</Typography>
            </Stack>
        </Stack>
    );
};

export default Footbar;