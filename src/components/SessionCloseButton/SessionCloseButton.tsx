import React, {FC, useEffect} from 'react';
import {Button, Stack, Typography} from "@mui/material";
import MonitorIcon from '@mui/icons-material/Monitor';
import {ISession} from "../../interfaces/ISession";
import {useRemoveSessionMutation} from "../../API/API";

interface ISessionCloseButton {
    session: ISession,
    setSession: Function,
}

const SessionCloseButton: FC<ISessionCloseButton> = ({session, setSession}) => {

    const [RemoveSession, {data}] = useRemoveSessionMutation();

    useEffect(() => {
        if(data) {
            setSession(data);
        }
    }, [data]);

    const handleRemove = () => {
        RemoveSession({
            id: session.id,
        })
    }

    return (
        <Stack alignItems={'center'} justifyContent={'space-between'} direction={"row"}>
            <Stack gap={2} alignItems={'center'} direction={'row'}>
                <MonitorIcon/>
                <Typography variant={'h6'}>{session.os}<br/>
                    {session.browser}</Typography>
            </Stack>
            <Button variant="contained"
                    sx={{
                        borderRadius: '20px', maxWidth: '400px', minHeight: '40px',
                        padding: '0 3rem', margin: '1rem 0'
                    }}
                    color={'info'}
                    onClick={handleRemove}>

                Отключить
            </Button>
        </Stack>
    );
};

export default SessionCloseButton;