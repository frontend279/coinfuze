import {Box, styled} from "@mui/material";

export const LeftMenuBox = styled(Box)(({ theme }) => ({
	backgroundColor: theme.palette.background.default,
	color: theme.palette.text.primary,
	borderRight: '1px solid white',
	maxWidth: "187px",
}))