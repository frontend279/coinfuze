import React, {FC} from 'react';
import {List, ListItemButton, ListItemIcon, ListItemText} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {LeftMenuBox} from "./styles";
import LinkIcon from '@mui/icons-material/Link';
import FileDownloadIcon from '@mui/icons-material/FileDownload';
import SettingsIcon from '@mui/icons-material/Settings';
import GppGoodIcon from '@mui/icons-material/GppGood';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import PersonIcon from '@mui/icons-material/Person';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import CampaignIcon from '@mui/icons-material/Campaign';
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';

const LeftMenu: FC<{setIsAdmin: Function}> = ({setIsAdmin}) => {

	let navigate = useNavigate();

	const AdminHandler = () => {
		navigate('/admin')
		setIsAdmin(true)
	}

	return (
		<LeftMenuBox>
			<List component="nav">
				<ListItemButton onClick={e => navigate('/')}>
					<ListItemIcon>
						<LinkIcon/>
					</ListItemIcon>
					<ListItemText primary="Панель управления"/>
				</ListItemButton>
				<ListItemButton onClick={e => navigate('/deposit')}>
					<ListItemIcon>
						<FileDownloadIcon/>
					</ListItemIcon>
					<ListItemText primary="Депозит"/>
				</ListItemButton>
				<ListItemButton onClick={e => navigate('/plan')}>
					<ListItemIcon>
						<SettingsIcon/>
					</ListItemIcon>
					<ListItemText primary="Управление планами"/>
				</ListItemButton>
				<ListItemButton onClick={e => navigate('/partner')}>
					<ListItemIcon>
						<GppGoodIcon/>
					</ListItemIcon>
					<ListItemText primary="Партнерская программа"/>
				</ListItemButton>
				<ListItemButton onClick={e => navigate('/withdraw')}>
					<ListItemIcon>
						<FileUploadIcon/>
					</ListItemIcon>
					<ListItemText primary="Вывод средств"/>
				</ListItemButton>
				<ListItemButton onClick={e => navigate('/profile')}>
					<ListItemIcon>
						<PersonIcon/>
					</ListItemIcon>
					<ListItemText primary="Личная информация"/>
				</ListItemButton>
				<ListItemButton >
					<ListItemIcon>
						<HelpOutlineIcon/>
					</ListItemIcon>
					<ListItemText primary="FAQ"/>
				</ListItemButton>
				<ListItemButton onClick={e => navigate('/news')}>
					<ListItemIcon>
						<CampaignIcon/>
					</ListItemIcon>
					<ListItemText primary="Новости"/>
				</ListItemButton>
				<ListItemButton onClick={e => AdminHandler()}>
					<ListItemIcon>
						<AdminPanelSettingsIcon/>
					</ListItemIcon>
					<ListItemText primary="Админ панель"/>
				</ListItemButton>
			</List>
		</LeftMenuBox>
	);
};

export default LeftMenu;