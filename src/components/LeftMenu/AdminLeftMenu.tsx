import React, {FC} from 'react';
import {List, ListItemButton, ListItemIcon, ListItemText} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {LeftMenuBox} from "./styles";
import CampaignIcon from '@mui/icons-material/Campaign';
import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import {useAppDispatch} from "../../redux";

const LeftMenu: FC<{setIsAdmin: Function}> = ({setIsAdmin}) => {

    let navigate = useNavigate();

    const dispatch = useAppDispatch();

    const AdminHandler = () => {
        navigate('/')
        setIsAdmin(false)
    }

    return (
        <LeftMenuBox>
            <List component="nav">
                <ListItemButton onClick={e => AdminHandler()}>
                    <ListItemIcon>
                        <AdminPanelSettingsIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Выход"/>
                </ListItemButton>
                <ListItemButton onClick={e => navigate('/admin/users')}>
                    <ListItemIcon>
                        <CampaignIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Пользователи"/>
                </ListItemButton>
                <ListItemButton onClick={e => navigate('/admin/plans')}>
                    <ListItemIcon>
                        <CampaignIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Инвест планы"/>
                </ListItemButton>
                <ListItemButton onClick={e => navigate('/admin/news')}>
                    <ListItemIcon>
                        <CampaignIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Новости"/>
                </ListItemButton>
            </List>
        </LeftMenuBox>
    );
};

export default LeftMenu;