import React from 'react';
import {Box, Button, Typography} from "@mui/material";
import {WithdrawStack} from "./styles";
import {useNavigate} from "react-router-dom";

const WithdrawNow = () => {

    const navigate = useNavigate()

    const buttonHandler = () => {
        navigate('/withdraw/proceed')
    }

    return (
        <WithdrawStack flex={1} color={'black'} direction={'column'} p={2}>
            <Typography variant={'h5'}>Снятие средств</Typography>
            <br />
            <Typography>На этой странице вы можете вывести средства на свой криптовалютный кошелек. Следуйте
                подсказкам, чтобы вывод был максимально быстрым и удобным.</Typography>
            <br />
            <Typography>• Вывести средства можно в той же криптовалюте, в которой вы пополнили депозит</Typography>
            <Typography>• Вывод может занять некоторое время, поскольку транзакции в сети криптовалют проходят с разной
                скоростью</Typography>
            <Typography>• Заявки на вывод принимаются автоматически</Typography>
            <Box flex={1} alignItems="end" display={'flex'}>
                <Button
                    variant="contained"
                    size={'large'}
                    sx={{borderRadius: '1rem', maxWidth: '300px', marginTop: '2rem'}}
                    color="success"
                    onClick={buttonHandler}>Начать снятие средств</Button>
            </Box>
        </WithdrawStack>
    );
};

export default WithdrawNow;