import {Box, Stack, styled} from "@mui/material";

export const RefProgramStack = styled(Stack)({
    backgroundColor: '#ceedf5',
    padding: '1rem 2rem'
})

export const RefLinkBox = styled(Box)({
    border: '1px solid black',
    borderRadius: '1rem',
    backgroundColor: 'white',
    padding: '0.5rem 1rem',
    margin: '1rem 0'
})