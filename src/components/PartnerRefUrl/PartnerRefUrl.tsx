import React, {useState} from 'react';
import {Box, Button, Divider, Stack, Typography} from "@mui/material";
import {RefLinkBox, RefProgramStack} from "./styles";

interface ISummary {
    allSummary: number;
    firstLvLSummary: number;
    secondLvLSummary: number;
    thirdLvLSummary: number;
}

const PartnerRefUrl = () => {

    const [summaryInfo, setSummaryInfo] = useState<ISummary>({
        allSummary: 0,
        firstLvLSummary: 0,
        secondLvLSummary: 0,
        thirdLvLSummary: 0,
    })

    const [refUrl, setRefUrl] = useState<string>('https://...')

    const aboutRefsButtonHandler = () => {
        console.log('aboutRefsButtonHandler')
    }


    return (
        <RefProgramStack flex={1} gap={2} direction={'column'}>
            <Stack gap={1} justifyContent={'space-between'} alignItems={'center'} direction={'row'}>
                <Typography variant={'h5'}>Ваша реферальная ссылка</Typography>
                <Button variant="contained"
                        size={'large'}
                        sx={{borderRadius: '20px', border: '2px solid black', maxWidth: '200px'}}
                        color={'success'}
                        onClick={aboutRefsButtonHandler}>Подробнее</Button>
            </Stack>
            <Divider color={'black'} />
            <Box flex={1}>
                <Typography variant={'body1'}>Каждый участник CoinFuze получает уникальную реферальную ссылку, которой можно делиться с
                    друзьями со всего мира. Просто скопируйте эту ссылку и отправьте тем, кого хотите пригласить в CoinFuze.</Typography>
                <RefLinkBox>
                    <Typography>{refUrl}</Typography>
                </RefLinkBox>
                <Box>
                    <Typography variant={'body2'}>• Делитесь партнерской ссылкой</Typography>
                    <Typography variant={'body2'}>• Изучайте статистику переходов по ней</Typography>
                    <Typography variant={'body2'}>• Следите за тем, сколько людей зарегистрировалось по вашей ссылке</Typography>
                    <Typography variant={'body2'}>• Наблюдайте за ростом вашей прибыли</Typography>
                </Box>
            </Box>
            <Divider color={'black'} />
            <Stack justifyContent={'space-around'} gap={1} direction={'row'}>
                <Stack direction={'column'}>
                    <Typography variant={'body1'}>Всего заработано</Typography>
                    <Typography variant={'body1'}>{summaryInfo.allSummary}</Typography>
                </Stack>
                <Divider color={'black'} orientation={'vertical'}/>
                <Stack  direction={'column'}>
                    <Typography variant={'body1'}>Доход от партнеров первой линии</Typography>
                    <Typography variant={'body1'}>{summaryInfo.firstLvLSummary}</Typography>
                </Stack>
                <Divider color={'black'} orientation={'vertical'}/>
                <Stack direction={'column'}>
                    <Typography variant={'body1'}>Доход от партнеров второй линии</Typography>
                    <Typography variant={'body1'}>{summaryInfo.secondLvLSummary}</Typography>
                </Stack>
                <Divider color={'black'} orientation={'vertical'}/>
                <Stack direction={'column'}>
                    <Typography variant={'body1'}>Доход от партнеров третьей линии</Typography>
                    <Typography variant={'body1'}>{summaryInfo.thirdLvLSummary}</Typography>
                </Stack>
            </Stack>
        </RefProgramStack>
    );
};

export default PartnerRefUrl;