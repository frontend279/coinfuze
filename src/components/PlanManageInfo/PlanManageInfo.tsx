import React from 'react';
import {Box, Button, Typography} from "@mui/material";
import {InfoStack} from "./styles";
import {useNavigate} from "react-router-dom";

const PlanManageInfo = () => {

    const navigate = useNavigate()

    const buttonHandler = () => {
        navigate('/deposit/proceed')
    }

    return (
        <InfoStack p={3} direction={'column'}>
            <Typography variant={'h6'}>Сделайте свои первые шаги в инвестировании</Typography>
            <br />
            <Typography variant={'body1'}>Приветствуем в CoinFuze Мы ради видеть вас в числе
                полноправных участников нашей системы</Typography>
            <br />
            <Typography variant={'body1'}>
                Начинайте свой путь инвестора прямо сейчас
            </Typography>
            <br />
            <Typography variant={'body1'}>
                Чтобы пользоваться всеми услугами CoinFuze, вам нужно пополнить депозит. Сделать это
                вы можете, перейдя во вкладку Депозит на приборной панели, или перейдя по ссылке ниже
            </Typography>
            <Box flex={1} alignItems="end" display={'flex'}>
                <Button
                    variant="contained"
                    size={'large'}
                    sx={{borderRadius: '1rem', maxWidth: '400px', marginTop: '2rem'}}
                    color="success"
                    onClick={buttonHandler}>Перейти к пополнению депозита</Button>
            </Box>
        </InfoStack>
    );
};

export default PlanManageInfo;