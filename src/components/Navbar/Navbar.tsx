import React, {FC, useEffect, useState} from 'react';
import {AppBar, Avatar, Divider, IconButton, Menu, Paper, Typography} from "@mui/material";
import {AccountAndInfo, CustomMenuItem, CustomToolBar} from "./styles";
import CoinfuzeLogo from '../../assets/img/coinfuze.png'
import NotificationsIcon from '@mui/icons-material/Notifications';
import SettingsIcon from '@mui/icons-material/Settings';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import {useAppSelector} from "../../redux";
import {useLogoutMutation} from "../../API/API";
import {getAvatarURL} from "../../Utils/Utils";

const Navbar: FC = () => {

	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

	const [logout, {}] = useLogoutMutation();

	const data = useAppSelector(selector => selector.UserDataSlice.accountInfo);
	const [avatarURL, setAvatarURL] = useState("");

	useEffect(() => {
		setAvatarURL(getAvatarURL(data.avatar));
	}, [data, ]);

	const buttonHandler = (event: React.MouseEvent<HTMLButtonElement>) => {
		if(!anchorEl) {
			setAnchorEl(event.currentTarget)
		} else {
			setAnchorEl(null)
		}
	}

	const logoutHandler = () => {
		logout()
		setAnchorEl(null)
	}

	return (
		<>
			<AppBar position="static">
				<CustomToolBar>
					<img alt={'coinfuze'} src={CoinfuzeLogo} />
					<AccountAndInfo>
						<IconButton onClick={buttonHandler} sx={{gap: '0.5rem', borderRadius: 0}}>
							<Avatar alt="user image" src={avatarURL}/>
							<Typography variant={'body1'}>{data.firstName}</Typography>
						</IconButton>
					</AccountAndInfo>
				</CustomToolBar>
			</AppBar>
			<Menu
				anchorEl={anchorEl}
				open={!!anchorEl}
				disableScrollLock={true}
				onClose={buttonHandler}
			>
				<Paper sx={{width: 320, maxWidth: '100%', boxShadow: 'none', p: '1rem 0'}}>
					<CustomMenuItem>
						<NotificationsIcon/>
						<Typography variant="body1">
							Уведомления
						</Typography>
					</CustomMenuItem>
					<CustomMenuItem>
						<SettingsIcon/>
						<Typography variant="body1">
							Настройки
						</Typography>
					</CustomMenuItem>
					<Divider/>
					<CustomMenuItem onClick={logoutHandler}>
						<ExitToAppIcon />
						<Typography variant="body1">
							Выход
						</Typography>
					</CustomMenuItem>
				</Paper>
			</Menu>
		</>

	);
};

export default Navbar;