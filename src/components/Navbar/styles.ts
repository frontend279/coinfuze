import {MenuItem, styled} from "@mui/material";
import {Toolbar} from "@mui/material";

export const CustomToolBar = styled(Toolbar)(({theme}) => ({
	display: 'flex',
	justifyContent: 'space-between',
	padding: '0 1rem',
	borderBottom: '1px solid white',
	backgroundColor: theme.palette.background.default
}))

export const AccountAndInfo = styled(Toolbar)(({theme}) => ({
	display: 'flex',
	flexDirection: 'row',
	justifyContent: 'center',
	alignItems: 'center',
}))

export const Info = styled(Toolbar)(({theme}) => ({
	display: 'flex',
	flexDirection: 'column',
	gap: '0.5rem',
}))

export const CustomMenuItem = styled(MenuItem)(({theme}) => ({
	gap: '0.5rem',
}))