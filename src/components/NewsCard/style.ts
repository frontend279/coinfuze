import {Card, CardActions, styled} from "@mui/material";

export const NewsBoxCard = styled(Card)(({theme}) => ({
	display: 'flex',
	flexDirection: 'column',
	maxWidth: 345,
	minHeight: 450, 
	padding: '1rem',
	backgroundColor: "rgb(206,237,245)",
	color: 'black',
}))

export const NewsCardActions = styled(CardActions)(({theme}) => ({
	flex: "1",
	justifyContent: 'center',
	alignItems: 'end',
	marginBottom: "1rem"
}))