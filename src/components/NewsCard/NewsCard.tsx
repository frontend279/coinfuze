import {Button, Card, CardActions, CardContent, CardMedia, Typography} from '@mui/material';
import React, {FC} from 'react';
import {NewsBoxCard, NewsCardActions} from "./style";

const NewsCard: FC = () => {
	return (
		<NewsBoxCard>
			<CardMedia
				component="img"
				alt="green iguana"
				height="250"
				image={'https://res.cloudinary.com/startup-grind/image/upload/c_fill,dpr_2.0,f_auto,g_center,h_1080,q_100,w_1080/v1/gcs/platform-data-byu/events/Open-For-Business_03_dtJkdGa.jpg'}
			/>
			<CardContent sx={{justifyContent: 'space-between'}}>
				<Typography gutterBottom variant="body2">
					21 февраль 2022
				</Typography>
				<Typography gutterBottom variant="h5" component="div">
					Сайт был создан
				</Typography>
				<Typography variant="body2">
					Это новость о том, что сайт был создан благодаря одному человеку
				</Typography>
			</CardContent>
			<NewsCardActions>
				<Button variant="contained" size={'large'} sx={{borderRadius: '1rem'}} color="success">Читать дальше</Button>
			</NewsCardActions>
		</NewsBoxCard>
	);
};

export default NewsCard;