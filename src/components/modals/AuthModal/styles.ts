import {Box, Modal, Stack, styled} from "@mui/material";

export const AuthM = styled(Modal)({
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backdropFilter: "blur(5px)",
})

export const AuthBox = styled(Box)({
    position: "absolute",
    backgroundColor: "#090909",
    color: "white",
    padding: "2.5rem",
    borderRadius: "15px",

})

export const RegFormStack = styled(Stack)({
    flexDirection: "column",
    gap: "1rem",
    textAlign: "center"
})

export const RegInfoStack = styled(Stack)({
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    gap: "0.4rem",
    padding: "0 2rem"
})

export const CreateStack = styled(Stack)({
    flexDirection: "column",
    gap: "0.7rem",
})

export const SocialMediaStack = styled(Stack)({
    flexDirection: "column",
    gap: "0.7rem",
})

export const SocialMediaItem = styled(Stack)({
    height: "50px",
    borderRadius: "100%",
})

export const RegLastFormStack = styled(Stack)({
    flexDirection: "column",
})
