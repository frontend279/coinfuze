import React, {FC, useState} from 'react';
import {Stack} from "@mui/material";
import RegFirstStep from "./RegForm/RegFirstStep";
import RegLastStep from "./RegForm/RegLastStep";
import {ICreateAccount} from "../../../interfaces/ICreateAccount";

const RegForm: FC<{setStage: Function}> = ({setStage}) => {

    const [regStage, setRegStage] = useState(0);
    const [account, setAccount] = useState({} as ICreateAccount)

    const getRegStage = () => {
        switch (regStage) {
            case 0:
                return <RegFirstStep account={account} setAccount={setAccount} setStage={setStage} setRegStage={setRegStage} />
            case 1:
                return <RegLastStep account={account} setAccount={setAccount} setRegStage={setRegStage}/>
        }
    }

    return (
        <Stack gap={3} direction={"row"}>
            {getRegStage()}
        </Stack>
    );
};

export default RegForm;