import React, {useState} from 'react';
import {AuthBox, AuthM} from "./styles";
import LoginForm from "./LoginForm";
import RegForm from "./RegForm";
import {useAppSelector} from "../../../redux";


const AuthModal = () => {

    const [stage, setStage] = useState(1);
    const {isShown} = useAppSelector(state => state.AuthModalSlice);

    const getStage = () => {
        switch (stage) {
            case 0:
                return <RegForm setStage={setStage} />
            case 1:
                return <LoginForm setStage={setStage} />

        }
    }

    return (
        <AuthM
            open={isShown}
        >
            <AuthBox>
                {getStage()}
            </AuthBox>
        </AuthM>
    );
};

export default AuthModal;