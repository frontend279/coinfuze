import React, {FC, useEffect, useState} from 'react';
import {CreateStack, RegFormStack, RegInfoStack, SocialMediaItem, SocialMediaStack} from "../styles";
import {Button, TextField, Typography} from "@mui/material";
import {SocialIcon} from "react-social-icons";
import {useAppDispatch} from "../../../../redux";
import * as Yup from "yup";
import {useFormik} from "formik";
import {ICreateAccount} from "../../../../interfaces/ICreateAccount";
import {useValidateRegisterMutation} from "../../../../API/API";
import {SerializedError} from "@reduxjs/toolkit";

const RegFirstStep:FC<{setStage: Function,
    setRegStage: Function,
    setAccount: Function,
    account:ICreateAccount}> = ({setStage, setRegStage, setAccount, account}) => {

    const [localError, setLocalError] = useState("")
    const dispatch = useAppDispatch()



    const [validateRegister, {data, error}] = useValidateRegisterMutation()

    useEffect(() => {
        const errorMsg = (error as SerializedError)?.message;
        if(errorMsg !== undefined) {
            setLocalError(errorMsg);
        }

    }, [error, ]);

    useEffect(() => {
        if(data) {
            if(data.validate) {
                setAccount({...account, login: formik.values.login, email: formik.values.email, password: formik.values.password})
                setRegStage(1)
            } else {
                setLocalError("Аккаунт с таким логином или email уже существует")
            }
        }
    }, [data])



    const swapStage = () => {
        setStage(1);
    }

    const validateSchema = Yup.object().shape({
        login: Yup.string().required("Это поле обязательно").min(5, "Мин длина логина - 5").max(16, "макс длина логина - 16"),
        email: Yup.string().email("Введите настоящий email").required("Это поле обязательно"),
        password: Yup.string()
            .required("Это поле обязательно")
            .min(6, "Мин длина пароля - 6"),
        confirmPassword: Yup.string().required("Это поле обязательно").test('password-match', 'Пароли не совпадают', function(value) {
            return value === this.parent.password;
        }),
    });

    const formik = useFormik({
        initialValues: {
            login: "",
            email: "",
            password: "",
            confirmPassword: "",
        },
        validationSchema: validateSchema,
        validateOnChange: false,
        onSubmit: (values, { resetForm }) => {
           validateRegister(values);
        },
    });

    return (
        <><RegFormStack direction={'column'} flex={1}>
            <Typography variant={"h3"}>Регистрация</Typography>
            <form onSubmit={formik.handleSubmit}>
                <CreateStack>
                    {localError.length !== 0
                        &&
                        <Typography color={'error'} variant={"body1"}>{localError}</Typography>}
                    <TextField value={formik.values.login}
                               autoComplete='off'
                               onChange={formik.handleChange}
                               name={"login"}
                               label="Логин"
                               variant="outlined"
                               error={!!formik.errors.login?.length}
                               helperText={formik.errors.login}/>
                    <TextField value={formik.values.email}
                               autoComplete='off'
                               name={"email"}
                               onChange={formik.handleChange}
                               error={!!formik.errors.email?.length}
                               helperText={formik.errors.email}
                               label="Email"
                               variant="outlined"/>
                    <TextField
                        autoComplete='off'
                        label="Пароль"
                        value={formik.values.password}
                        name={"password"}
                        type={"password"}
                        onChange={formik.handleChange}
                        error={!!formik.errors.password?.length}
                        helperText={formik.errors.password}
                        variant="outlined"/>
                    <TextField
                        autoComplete='off'
                        type={"password"}
                        label="Повтор пароля"
                        name={"confirmPassword"}
                        onChange={formik.handleChange}
                        error={!!formik.errors.confirmPassword?.length}
                        helperText={formik.errors.confirmPassword}
                        value={formik.values.confirmPassword}
                        variant="outlined"/>
                    <Button type={"submit"} color={"success"}
                            variant={"contained"}>Далее</Button>
                </CreateStack>
            </form>
            <Typography variant={'body1'} color={"lightgrey"}>или</Typography>
            <SocialMediaStack>
                <SocialMediaItem>
                    <SocialIcon network={'google'}></SocialIcon>
                </SocialMediaItem>
            </SocialMediaStack>
        </RegFormStack><RegInfoStack direction={'column'} flex={1}>
            <Typography mb={1} align={"center"} variant="h4">Приветствуем на проекте</Typography>
            <Typography noWrap={true}>Уже есть аккаунт? Тогда скорее заходи :)</Typography>
            <Button color={"success"} variant={"contained"} onClick={swapStage}>Войти в аккаунт</Button>
        </RegInfoStack></>
    );
};

export default RegFirstStep;