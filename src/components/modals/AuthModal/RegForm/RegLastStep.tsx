import React, {FC, useEffect, useState} from 'react';
import {CreateStack, RegLastFormStack} from "../styles";
import {Button, Stack, TextField, Typography} from "@mui/material";
import * as Yup from "yup";
import {useFormik} from "formik";
import {ICreateAccount} from "../../../../interfaces/ICreateAccount";
import {DateField, LocalizationProvider} from "@mui/x-date-pickers";
import {AdapterDateFns} from "@mui/x-date-pickers/AdapterDateFnsV3";
import {useRegisterMutation} from "../../../../API/API";
import {SerializedError} from "@reduxjs/toolkit";
import {useAppDispatch} from "../../../../redux";
import {UserDataActions} from "../../../../redux/slices/user/UserDataSlice";

const RegLastStep:FC<{setRegStage: Function,
    setAccount: Function,
    account: ICreateAccount}> = ({setRegStage, setAccount, account}) => {

    const [localError, setLocalError] = useState("")
    const dispatch = useAppDispatch()

    const [createAccount, {data, error}] = useRegisterMutation()


    useEffect(() => {
        const errorMsg = (error as SerializedError)?.message;
        if(errorMsg !== undefined) {
            setLocalError(errorMsg);
        }
    }, [error, ]);

    useEffect(() => {
        if(data) {
            dispatch(UserDataActions.setUserData(data))
            dispatch(UserDataActions.setUserToken({
                access_token: data.access_token,
                refresh_token: data.refresh_token,
            }))
            formik.resetForm()
            setRegStage(0)
        }
    }, [data, ])



    const phoneRegExp = /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/
    const validateSchema = Yup.object().shape({
        firstName: Yup.string().required("Это поле обязательно"),
        lastName: Yup.string().required("Это поле обязательно"),
        middleName: Yup.string().required("Это поле обязательно"),
        country: Yup.string().required("Это поле обязательно"),
        city: Yup.string().required("Это поле обязательно"),
        addressF: Yup.string().required("Это поле обязательно"),
        postalCode: Yup.string().required("Это поле обязательно"),
        birthday: Yup.string().required("Это поле обязательно"),
        addressL: Yup.string().required("Это поле обязательно"),
        phone: Yup.string().required("Это поле обязательно").matches(phoneRegExp, 'Введите правильный номер телефона'),
    });
    const back = () => {
        setRegStage(0)
    }

    const formik = useFormik({
        initialValues: {
            firstName: "",
            lastName: "",
            middleName: "",
            city: "",
            country: "",
            addressF: "",
            addressL: "",
            phone: "",
            postalCode: "",
            birthday: "",
        },
        validationSchema: validateSchema,
        validateOnChange: false,
        validateOnMount: false,
        validateOnBlur: false,
        onSubmit: (values, { resetForm }) => {
            const data: ICreateAccount = {...account, userInfo: {
                firstName: values.firstName,
                    lastName: values.lastName, middleName: values.middleName,
                    city: values.city, country: values.country,
                    addressFirst: values.addressF, addressLast: values.addressL, phone: values.phone, birthday: values.birthday,
                    postalCode: values.postalCode
                }}
            createAccount(data);
        },
    });

    return (
        <>
        <Stack gap={2} direction={"column"}>
            <Typography variant={"h3"}>Регистрация</Typography>
            {localError.length !== 0
                &&
                <Typography color={'error'} variant={"body1"}>{localError}</Typography>}
            <form onSubmit={formik.handleSubmit}>
                <Stack direction={'column'}>
                <Stack minWidth={"800px"} gap={"3rem"} direction={"row"}>
                    <RegLastFormStack justifyContent={'space-between'} direction={'column'} flex={1}>
                        <CreateStack>
                            <TextField value={formik.values.lastName}
                                       autoComplete='off'
                                       onChange={formik.handleChange}
                                       name={"lastName"}
                                       label="Фамилия"
                                       variant="outlined"
                                       error={!!formik.errors.lastName?.length}
                                       helperText={formik.errors.lastName}/>
                            <TextField value={formik.values.firstName}
                                       autoComplete='off'
                                       name={"firstName"}
                                       onChange={formik.handleChange}
                                       error={!!formik.errors.firstName?.length}
                                       helperText={formik.errors.firstName}
                                       label="Имя" variant="outlined"/>
                            <TextField
                                autoComplete='off'
                                label="Отчество"
                                value={formik.values.middleName}
                                name={"middleName"}
                                onChange={formik.handleChange}
                                error={!!formik.errors.middleName?.length}
                                helperText={formik.errors.middleName}
                                variant="outlined"/>
                            <TextField
                                autoComplete='off'
                                label="Телефон"
                                name={"phone"}
                                onChange={formik.handleChange}
                                error={!!formik.errors.phone?.length}
                                helperText={formik.errors.phone}
                                value={formik.values.phone}
                                variant="outlined"/>
                            <LocalizationProvider dateAdapter={AdapterDateFns}>
                                <DateField
                                    disableFuture
                                    label="Дата рождения"
                                    format="dd/MM/yyyy"
                                    value={formik.values.birthday}
                                    onChange={(value) => formik.setFieldValue("birthday", value, false)}
                                    slotProps={{
                                        textField: {
                                            variant: "outlined",
                                            error: !!formik.errors.birthday,
                                            helperText: formik.errors.birthday
                                        }
                                    }}
                                />
                            </LocalizationProvider>
                        </CreateStack>

                    </RegLastFormStack>
                    <RegLastFormStack justifyContent={'space-between'} direction={'column'} flex={1}>
                        <TextField
                            autoComplete='off'
                            label="Страна"
                            name={"country"}
                            onChange={formik.handleChange}
                            error={!!formik.errors.country?.length}
                            helperText={formik.errors.country}
                            value={formik.values.country}
                            variant="outlined"/>
                        <TextField value={formik.values.city}
                                   autoComplete='off'
                                   onChange={formik.handleChange}
                                   name={"city"}
                                   label="Город"
                                   variant="outlined"
                                   error={!!formik.errors.city?.length}
                                   helperText={formik.errors.city}/>
                        <TextField value={formik.values.addressF}
                                   autoComplete='off'
                                   onChange={formik.handleChange}
                                   name={"addressF"}
                                   label="Адрес 1"
                                   variant="outlined"
                                   error={!!formik.errors.addressF?.length}
                                   helperText={formik.errors.addressF}/>
                        <TextField value={formik.values.addressL}
                                   autoComplete='off'
                                   onChange={formik.handleChange}
                                   name={"addressL"}
                                   label="Адрес 2"
                                   variant="outlined"
                                   error={!!formik.errors.addressL?.length}
                                   helperText={formik.errors.addressL}/>
                        <TextField
                            label="Почтовый код"
                            name={"postalCode"}
                            onChange={formik.handleChange}
                            autoComplete='off'
                            error={!!formik.errors.postalCode?.length}
                            helperText={formik.errors.postalCode}
                            value={formik.values.postalCode}
                            variant="outlined"/>
                    </RegLastFormStack>
                </Stack>
                    <Stack mt={2} direction={"row"} justifyContent={'center'} flex={1} gap={2}>
                        <Button onClick={back} color={"success"}
                                variant={"contained"}>Назад</Button>
                        <Button type={"submit"} color={"success"}
                                variant={"contained"}>Зарегистрировать</Button>
                    </Stack>
                </Stack>
            </form>
        </Stack>
</>
);
};

export default RegLastStep;