import React, {FC, useEffect, useState} from 'react';
import {CreateStack, RegFormStack, RegInfoStack, SocialMediaItem, SocialMediaStack} from "./styles";
import {Button, Stack, TextField, Typography} from "@mui/material";
import {SocialIcon} from "react-social-icons";
import {useAppDispatch} from "../../../redux";
import * as Yup from "yup";
import {useFormik} from "formik";
import {SerializedError} from "@reduxjs/toolkit";
import {UserDataActions} from "../../../redux/slices/user/UserDataSlice";
import {useLoginMutation} from "../../../API/API";

const LoginForm: FC<{setStage: Function}> = ({setStage}) => {
    const [localError, setLocalError] = useState("");
    const dispatch = useAppDispatch()


    const swapStage = () => {
        setStage(0);
    }

    const [login, {error, data}] = useLoginMutation();


    useEffect(() => {
        const errorMsg = (error as SerializedError)?.message;
        if(errorMsg !== undefined) {
            setLocalError(errorMsg);
        }
    }, [error, ]);

    useEffect(() => {
        if(data) {
            dispatch(UserDataActions.setUserData(data))
            dispatch(UserDataActions.setUserToken({
                access_token: data.access_token,
                refresh_token: data.refresh_token,
            }))
            formik.resetForm()
        }
    }, [data, ])

    const validateSchema = Yup.object().shape({
        login: Yup.string().required("Это поле обязательно").min(5, "Мин длина логина - 5").max(16, "макс длина логина - 16"),
        password: Yup.string()
            .required("Это поле обязательно")
            .min(6, "Мин длина пароля - 6"),
    });

    const formik = useFormik({
        initialValues: {
            login: "",
            password: "",
        },
        validationSchema: validateSchema,
        validateOnChange: false,
        onSubmit: (values, { resetForm }) => {
            login({login: values.login, password: values.password});
        },
    });

    return (
        <Stack gap={3} direction={"row"}>
            <RegFormStack direction={'column'} flex={1}>
                <Typography variant={"h3"}>Вход</Typography>
                <form onSubmit={formik.handleSubmit}>
                    <CreateStack>
                        {
                            localError.length !== 0
                            &&
                            <Typography color={'error'} variant={"body1"}>{localError.toString()}</Typography>
                        }
                        <TextField value={formik.values.login}
                                   onChange={formik.handleChange}
                                   name="login"
                                   label="Логин или почта" variant="outlined"/>
                        <TextField
                            label="Пароль"
                            name="password"
                            value={formik.values.password}
                            type={"password"}
                            onChange={formik.handleChange}
                            variant="outlined"/>
                        <Button type={"submit"} color={"success"}
                                variant={"contained"}>Вход</Button>
                    </CreateStack>
                </form>
                <Typography variant={'body1'} color={"lightgrey"}>или</Typography>
                <SocialMediaStack>
                    <SocialMediaItem>
                        <SocialIcon network={'google'}></SocialIcon>
                    </SocialMediaItem>
                </SocialMediaStack>
            </RegFormStack>
            <RegInfoStack direction={'column'} flex={1}>
                <Typography mb={1} align={"center"} variant="h4">Приветствуем на проекте</Typography>
                <Typography noWrap={true}>Новичок? Тогда беги создавать аккаунт.</Typography>
                <Button color={"success"} variant={"contained"} onClick={swapStage}>Создать аккаунт</Button>
            </RegInfoStack>
        </Stack>
    );
};

export default LoginForm;