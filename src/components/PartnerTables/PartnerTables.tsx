import React from 'react';
import InvitedRefs from "./Tables/InvitedRefs";
import FirstLvL from "./Tables/FirstLvL";
import SecondLvL from "./Tables/SecondLvL";
import ThirdLvL from "./Tables/ThirdLvL";

const PartnerTables = () => {
    return (
        <>
            <InvitedRefs />
            <FirstLvL />
            <SecondLvL />
            <ThirdLvL />
        </>
    );
};

export default PartnerTables;