import {
    Box,
    InputBase,
    Stack,
    styled,
    TableCell,
    tableCellClasses,
    TablePagination,
    tablePaginationClasses
} from "@mui/material";

export const StructureStack = styled(Stack)({
    backgroundColor: '#a6e1f2',
    padding: '1rem 2rem'
})

export const SearchBox = styled(Box)({
    border: '1px solid black',
    borderRadius: '1rem',
    padding: '0.5rem 1rem',
    backgroundColor: 'white',
    height: '20px',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    width: "700px",
})

export const SearchInputBase = styled(InputBase)({
    color: 'black',
    marginLeft: '0.3rem'
})

export const RefTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        color: theme.palette.common.black,
    },
    [`&.${tableCellClasses.body}`]: {
        color: theme.palette.common.black,
    },
}));

export const SearchTablePagination = styled(TablePagination)(({theme}) => ({
    borderBottom: 'none',
    [`&.${tablePaginationClasses.root}`]: {
        color: theme.palette.common.black,
    },
    '& .MuiSvgIcon-root': {
        color: theme.palette.common.black,
    },
}))