import React, {useState} from 'react';
import {Divider, Stack, Table, TableBody, TableHead, TableRow, Typography} from "@mui/material";
import {RefTableCell, SearchTablePagination, StructureStack} from "../styles";
import {IRefs} from "../../../interfaces/IRefs";
import {HistoryTableCell, HistoryTablePagination} from "../../DepositTable/styles";

const SecondLvL = () => {

    const [secondLvLRows, setSecondLvLRows] = useState<IRefs[]>([])
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);


    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);


    };

    return (
        <StructureStack gap={2} direction={'column'}>
            <Stack direction={'row'}>
                <Stack direction={'column'}>
                    <Typography variant={'h6'}>Уровень 2 - комиссия от депозита и прибыли 2%</Typography>
                </Stack>
            </Stack>
            <Divider color={'black'}/>
            <Table sx={{ minWidth: 650 }}>
                <TableHead >
                    <TableRow >
                        <RefTableCell align="center">Дата регистрации</RefTableCell>
                        <RefTableCell align="center">Имя пользователя/ID</RefTableCell>
                        <RefTableCell align="center">К-ство партнеров</RefTableCell>
                        <RefTableCell align="center">Доход от депозитов</RefTableCell>
                        <RefTableCell align="center">Доход от прибыли</RefTableCell>
                        <RefTableCell align="center">Ваш доход</RefTableCell>
                    </TableRow>
                </TableHead>
                {secondLvLRows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(secondLvLRow => (
                        <TableBody>
                            <HistoryTableCell align="center">{secondLvLRow.regDate}</HistoryTableCell>
                            <HistoryTableCell align="center">{secondLvLRow.userName}/{secondLvLRow.ID}</HistoryTableCell>
                            <HistoryTableCell align="center">{secondLvLRow.partnerAmount}</HistoryTableCell>
                            <HistoryTableCell align="center">{secondLvLRow.depositSummary}</HistoryTableCell>
                            <HistoryTableCell align="center">{secondLvLRow.plansSummary}</HistoryTableCell>
                            <HistoryTableCell align="center">{secondLvLRow.allSummary}</HistoryTableCell>
                        </TableBody>
                    ))
                }
                {
                    !secondLvLRows.length && (
                        <TableBody>
                            <HistoryTableCell colSpan={6} align="center">Нет данных</HistoryTableCell>
                        </TableBody>
                    )
                }
                <HistoryTablePagination
                    sx={{color: 'white'}}
                    rowsPerPageOptions={[5, 10, 15]}
                    count={secondLvLRows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    labelRowsPerPage={'Кол-во строк:'}
                />
            </Table>
        </StructureStack>
    );
};

export default SecondLvL;