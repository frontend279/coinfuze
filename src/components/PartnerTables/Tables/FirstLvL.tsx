import React, {useState} from 'react';
import {Divider, Stack, Table, TableBody, TableHead, TableRow, Typography} from "@mui/material";
import {RefTableCell, SearchTablePagination, StructureStack} from "../styles";
import {IRefs} from "../../../interfaces/IRefs";
import {HistoryTableCell, HistoryTablePagination} from "../../DepositTable/styles";

const FirstLvL = () => {

    const [firstLvLRows, setFirstLvLRows] = useState<IRefs[]>([])
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);


    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);


    };

    return (
        <StructureStack gap={2} direction={'column'}>
            <Stack direction={'row'}>
                <Stack direction={'column'}>
                    <Typography variant={'h6'}>Уровень 1 - комиссия от депозита и прибыли 3%</Typography>
                </Stack>
            </Stack>
            <Divider color={'black'}/>
            <Table sx={{ minWidth: 650 }}>
                <TableHead >
                    <TableRow >
                        <RefTableCell align="center">Дата регистрации</RefTableCell>
                        <RefTableCell align="center">Имя пользователя/ID</RefTableCell>
                        <RefTableCell align="center">К-ство партнеров</RefTableCell>
                        <RefTableCell align="center">Доход от депозитов</RefTableCell>
                        <RefTableCell align="center">Доход от прибыли</RefTableCell>
                        <RefTableCell align="center">Ваш доход</RefTableCell>
                    </TableRow>
                </TableHead>
                {firstLvLRows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(firstLvLRow => (
                        <TableBody>
                            <HistoryTableCell align="center">{firstLvLRow.regDate}</HistoryTableCell>
                            <HistoryTableCell align="center">{firstLvLRow.userName}/{firstLvLRow.ID}</HistoryTableCell>
                            <HistoryTableCell align="center">{firstLvLRow.partnerAmount}</HistoryTableCell>
                            <HistoryTableCell align="center">{firstLvLRow.depositSummary}</HistoryTableCell>
                            <HistoryTableCell align="center">{firstLvLRow.plansSummary}</HistoryTableCell>
                            <HistoryTableCell align="center">{firstLvLRow.allSummary}</HistoryTableCell>
                        </TableBody>
                    ))
                }
                {
                    !firstLvLRows.length && (
                        <TableBody>
                            <HistoryTableCell colSpan={6} align="center">Нет данных</HistoryTableCell>
                        </TableBody>
                    )
                }
                <HistoryTablePagination
                    sx={{color: 'white'}}
                    rowsPerPageOptions={[5, 10, 15]}
                    count={firstLvLRows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    labelRowsPerPage={'Кол-во строк:'}
                />
            </Table>
        </StructureStack>
    );
};

export default FirstLvL;