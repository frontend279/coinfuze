import React, {useState} from 'react';
import {RefTableCell, SearchTablePagination, StructureStack} from "../styles";
import {Divider, Stack, Table, TableBody, TableHead, TableRow, Typography} from "@mui/material";
import {HistoryTableCell, HistoryTablePagination} from "../../DepositTable/styles";
import {IRefs} from "../../../interfaces/IRefs";

const ThirdLvL = () => {

    const [thirdLvLRows, setThirdLvLRows] = useState<IRefs[]>([])
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);


    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);


    };

    return (
        <StructureStack gap={2} direction={'column'}>
            <Stack direction={'row'}>
                <Stack direction={'column'}>
                    <Typography variant={'h6'}>Уровень 3 - комиссия от депозита и прибыли 1%</Typography>
                </Stack>
            </Stack>
            <Divider color={'black'}/>
            <Table sx={{ minWidth: 650 }}>
                <TableHead >
                    <TableRow >
                        <RefTableCell align="center">Дата регистрации</RefTableCell>
                        <RefTableCell align="center">Имя пользователя/ID</RefTableCell>
                        <RefTableCell align="center">К-ство партнеров</RefTableCell>
                        <RefTableCell align="center">Доход от депозитов</RefTableCell>
                        <RefTableCell align="center">Доход от прибыли</RefTableCell>
                        <RefTableCell align="center">Ваш доход</RefTableCell>
                    </TableRow>
                </TableHead>
                {thirdLvLRows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(thirdLvLRow => (
                        <TableBody>
                            <HistoryTableCell align="center">{thirdLvLRow.regDate}</HistoryTableCell>
                            <HistoryTableCell align="center">{thirdLvLRow.userName}/{thirdLvLRow.ID}</HistoryTableCell>
                            <HistoryTableCell align="center">{thirdLvLRow.partnerAmount}</HistoryTableCell>
                            <HistoryTableCell align="center">{thirdLvLRow.depositSummary}</HistoryTableCell>
                            <HistoryTableCell align="center">{thirdLvLRow.plansSummary}</HistoryTableCell>
                            <HistoryTableCell align="center">{thirdLvLRow.allSummary}</HistoryTableCell>
                        </TableBody>
                    ))
                }
                {
                    !thirdLvLRows.length && (
                        <TableBody>
                            <HistoryTableCell colSpan={6} align="center">Нет данных</HistoryTableCell>
                        </TableBody>
                    )
                }
                <HistoryTablePagination
                    sx={{color: 'white'}}
                    rowsPerPageOptions={[5, 10, 15]}
                    count={thirdLvLRows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    labelRowsPerPage={'Кол-во строк:'}
                />
            </Table>
        </StructureStack>
    );
};

export default ThirdLvL;