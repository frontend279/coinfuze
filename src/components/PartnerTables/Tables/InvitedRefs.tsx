import React, {useEffect, useRef, useState} from 'react';
import {Box, Divider, Stack, Table, TableBody, TableHead, TableRow, Typography} from "@mui/material";
import {RefTableCell, SearchBox, SearchInputBase, StructureStack} from "../styles";
import SearchIcon from "@mui/icons-material/Search";
import {IRefs} from "../../../interfaces/IRefs";



const InvitedRefs = () => {

    const [search, setSearch] = useState<string>('')
    const [allRefs, setAllRefs] = useState<IRefs[]>([
        {
            regDate: '2023.23.04',
            userName: 'Моджи',
            ID: 3441431,
            partnerAmount: 43,
            depositSummary: '23 USDT',
            plansSummary: '11 USDT',
            allSummary: '34 USDT',
        },
        {
            regDate: '2023.23.04',
            userName: 'апк',
            ID: 3441431,
            partnerAmount: 43,
            depositSummary: '23 USDT',
            plansSummary: '11 USDT',
            allSummary: '34 USDT',
        },
    ])

    const [filteredAllRefs, setFilteredAllRef] = useState<IRefs[]>([])

    useEffect(() => {
        setFilteredAllRef([...allRefs].filter(ref => ref.ID.toString().toLowerCase().includes(search.toLowerCase()) ||
            ref.userName.toLowerCase().includes(search.toLowerCase())))
    }, [allRefs, search])

    const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value)
    }

    return (
        <StructureStack gap={2} direction={'column'}>
            <Stack direction={'row'}>
                <Stack direction={'column'}>
                    <Typography variant={'h6'}>Приглашенные рефералы</Typography>
                    <Typography variant={'body1'}>Посмотрите, кто уже зарегистрировался по вашей
                        партнерской ссылке, и сколько дохода вы получили. Не забывайте, что у нас трехуровневая реферальная программа
                        Вы получаете бонусы не только за тех, кого пригласили в CoinFuze, но и за участников, которых пригласили ваши рефералы</Typography>
                </Stack>
                <SearchBox>
                    <Box>
                        <SearchIcon />
                    </Box>
                    <SearchInputBase
                        placeholder="Поиск…"
                        value={search}
                        onChange={handleSearch}
                    />
                </SearchBox>
            </Stack>
            <Divider color={'black'}/>
            <Table sx={{ minWidth: 650 }}>
                <TableHead >
                    <TableRow >
                        <RefTableCell align="center">Дата регистрации</RefTableCell>
                        <RefTableCell align="center">Имя пользователя/ID</RefTableCell>
                        <RefTableCell align="center">К-ство партнеров</RefTableCell>
                        <RefTableCell align="center">Доход от депозитов</RefTableCell>
                        <RefTableCell align="center">Доход от прибыли</RefTableCell>
                        <RefTableCell align="center">Ваш доход</RefTableCell>
                    </TableRow>
                </TableHead>
                {   filteredAllRefs.map(ref => (
                    <TableBody>
                        <RefTableCell align="center">{ref.regDate}</RefTableCell>
                        <RefTableCell align="center">
                            {ref.userName}/{ref.ID}
                        </RefTableCell>
                        <RefTableCell align="center">{ref.partnerAmount}</RefTableCell>
                        <RefTableCell align="center">{ref.depositSummary}</RefTableCell>
                        <RefTableCell align="center">{ref.plansSummary}</RefTableCell>
                        <RefTableCell align="center">{ref.allSummary}</RefTableCell>
                    </TableBody>
                    ))
                }
            </Table>
        </StructureStack>
    );
};

export default InvitedRefs;