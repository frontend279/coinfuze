import React, {useState} from 'react';
import {Box, Button, Divider, Stack, Typography} from "@mui/material";
import {BackIconButton, DipositProceedStack, QRCodeImg, WalletIDBox, WalletIDTextField} from "./styles";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import {useNavigate} from "react-router-dom";

const DepositProceedMain = () => {

    const navigate = useNavigate()

    const [qrCodeUrl, setQRCodeUrl] = useState('URL')
    const [summary, setSummary] = useState('30$')
    const [status, setStatus] = useState('В ожидании')
    const [walletID, setWalletID] = useState('jfgndsjJNFSDBhkjhfbsdh34y52bsdhbSDt')

    const [userSummary, setUserSummary] = useState('')
    const [userWalletID, setUserWalletID] = useState('')

    const backButtonHandler = () => {
        navigate('/deposit')
    }

    const submitHandler = () => {

        //TODO: API отправки
    }

    return (
        <DipositProceedStack gap={2} p={2} mt={2} direction={'column'}>
            <Stack gap={2} direction={'row'} alignItems={'center'}>
                <BackIconButton onClick={backButtonHandler}>
                    <ArrowBackIcon fontSize={'large'}/>
                </BackIconButton>
                <Typography variant={'h5'}>Tether</Typography>
            </Stack>
            <Divider color={'black'}/>
            <Typography variant={'body1'}>Чтобы вывести средства на ваш кошелек, добавьте его в ваш
                аккаунт и создайте заявку на вывод</Typography>
            <Stack gap={3} direction={'row'}>
                <Stack direction={'column'}>
                    <QRCodeImg src={qrCodeUrl} />
                </Stack>
                <Stack justifyContent={'space-between'} gap={2} direction={'column'}>
                    <Stack gap={2} direction={'row'}>
                        <Typography variant={'h5'}>{summary}</Typography>
                        <Divider orientation={'vertical'} color={'black'} />
                        <Typography variant={'h5'}>{status}</Typography>
                    </Stack>
                    <Box>
                        <Typography variant={'body1'}>Депозит на этот WalletID</Typography>
                        <WalletIDBox mt={1}>{walletID}</WalletIDBox>
                    </Box>
                </Stack>
            </Stack>
            <Stack mt={1} gap={2} direction={'row'}>
                <WalletIDTextField onChange={e => setUserSummary(e.currentTarget.value)}
                                   label="Сумма пополнения"
                                   variant="filled"
                                    value={userSummary}/>
                <WalletIDTextField
                    label="ID кошелька"
                    variant="filled"
                    onChange={e => setUserWalletID(e.currentTarget.value)}
                    value={userWalletID}/>
                <Button variant="contained"
                        size={'medium'}
                        sx={{borderRadius: '20px', alignSelf: 'center', padding: '0.5rem 2rem'}}
                        color={'success'}
                        onClick={submitHandler}>
                    Отправить заявку
                </Button>
            </Stack>
        </DipositProceedStack>
    );
};

export default DepositProceedMain;