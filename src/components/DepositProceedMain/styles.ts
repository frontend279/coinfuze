import {
    Box,
    IconButton,
    nativeSelectClasses,
    Stack,
    styled,
    TableCell,
    tableCellClasses,
    TablePagination,
    tablePaginationClasses, TextField
} from "@mui/material";

export const DipositProceedStack = styled(Stack)(({theme}) => ({
    backgroundColor: '#9ad8f0'
}));

export const BackIconButton = styled(IconButton)(({theme}) => ({
    color: theme.palette.common.black
}))

export const HistoryStack = styled(Stack)({
    backgroundColor: '#9ad8f0',
    color: 'black',
})



export const QRCodeImg = styled('img')(({theme}) => ({
    width: '200px',
    height: '200px'
}))

export const WalletIDTextField = styled(TextField)(({theme}) => ({
    borderRadius: '2rem',
    maxWidth: '30rem',
    color: theme.palette.common.black,
    "& .MuiInputBase-input": {
        color: theme.palette.common.black,
    },
    "& .MuiInputLabel-root": {
        color: theme.palette.common.black,
    }
}))

export const WalletIDBox = styled(Box)(({theme}) => ({
    border: '1px solid black',
    borderRadius: '2rem',
    backgroundColor: 'white',
    display: 'flex',
    alignItems: 'center',
    padding: '0.5rem 1.3rem'
}))