import React, {FC} from 'react';
import {Button, Divider, Stack, Typography} from "@mui/material";
import {BackIconButton, SummaryTextField, WalletIDTextField} from "./styles";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import FileUploadIcon from '@mui/icons-material/FileUpload';

const ChooseWallet: FC = () => {
	return (
		<>
			<Stack gap={2} direction={'row'} alignItems={'center'}>
				<BackIconButton>
					<ArrowBackIcon fontSize={'large'}/>
				</BackIconButton>
				<Typography variant={'h5'}>Выберите нужный кошелек для вывода</Typography>
			</Stack>
			<Divider color={'black'}/>
			<Typography variant={'body1'}>Чтобы вывести средства на ваш кошелек, добавьте его в ваш
				аккаунт и создайте заявку на вывод</Typography>
			<Stack  direction={'column'}>
				<Typography variant={'body1'}>Вывод на этот WalletID:</Typography>
				<WalletIDTextField label="Номер" variant="filled" />
				<Stack mt={2} direction={'row'}>
					<SummaryTextField label="Сумма" variant="filled" />
					<Button variant="contained"
									size={'large'}
									sx={{borderRadius: '20px', maxWidth: '400px', 
									marginLeft: '1rem', padding: '0 3rem', gap: '0.4rem'}}
									color={'success'}>
						<FileUploadIcon />
						Отправить заявку
					</Button>
				</Stack>
			</Stack>
		</>
	);
};

export default ChooseWallet;