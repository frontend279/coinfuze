import React, {FC} from 'react';
import {Button, Divider, MenuItem, Stack, Typography} from "@mui/material";
import {BackIconButton, ProceedSelect, ProceedSelectInput} from "./styles";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import tetherIco from "../../assets/img/tether.svg";
import {useNavigate} from "react-router-dom";
import {useDispatch} from "react-redux";


const ChooseBalance: FC<{setState: Function}> = ({setState}) => {

	const navigate = useNavigate()

	const backToPrevHandler = () => {
		navigate(-1)
	}

	const dispatch = useDispatch()

	const submitHandler = () => {
		setState
	}

	return (
		<>
			<Stack gap={2} direction={'row'} alignItems={'center'}>
				<BackIconButton onClick={backToPrevHandler}>
					<ArrowBackIcon fontSize={'large'} />
				</BackIconButton>
				<Typography variant={'h5'}>Выберите баланс своего счета</Typography>
			</Stack>
			<Divider color={'black'}/>
			<Typography variant={'body1'}>Чтобы вывести средства на ваш кошелек, добавьте его в ваш
				аккаунт и создайте заявку на вывод</Typography>
			<Stack direction={'column'}>
				<ProceedSelect
					value={10}
					input={<ProceedSelectInput/>}
				>
					<MenuItem value={10}>
						<img src={tetherIco} width="30" alt={'tether'}/>
						<Stack flex={1} justifyContent='center' direction={'row'}>
							<Typography alignSelf={'center'} variant={'h6'}>Tether (USDT)</Typography>
						</Stack>
					</MenuItem>
				</ProceedSelect>
				<Typography mt={1} variant={'body2'}>Пожалуйста, добавьте информацию о выплате</Typography>
				<Button variant="contained"
								size={'large'}
								sx={{borderRadius: '20px', maxWidth: '200px', marginTop: '1rem'}}
								color={'success'}
								onClick={submitHandler}>Добавить</Button>
			</Stack>
		</>
	);
};

export default ChooseBalance;