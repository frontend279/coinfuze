import {
	Alert, IconButton,
	InputBase,
	inputBaseClasses,
	nativeSelectClasses, Select, Stack,
	styled, TableCell,
	tableCellClasses,
	TablePagination,
	tablePaginationClasses, TextField
} from "@mui/material";


export const WithdrawProceedStack = styled(Stack)(({theme}) => ({
	backgroundColor: '#9ad8f0'
}));

export const ProceedSelectInput = styled(InputBase)(({theme}) => ({
	'& .MuiInputBase-input': {
		display: 'flex',
		alignItems: 'center',
	}
}));

export const ProceedSelect = styled(Select)(({theme}) => ({
	maxWidth: '300px',
	color: theme.palette.common.black,
	[`& .MuiSelect-icon`]: {
		fill: theme.palette.common.black,
	},
}))




export const BackIconButton = styled(IconButton)(({theme}) => ({
	color: theme.palette.common.black
})) 

export const WalletIDTextField = styled(TextField)(({theme}) => ({
	borderRadius: '2rem',
	maxWidth: '30rem',
	color: theme.palette.common.black,
	marginTop: '1rem',
	"& .MuiInputBase-input": {
		color: theme.palette.common.black,
	},
	"& .MuiInputLabel-root": {
		color: theme.palette.common.black,
	}
}))

export const SummaryTextField = styled(TextField)(({theme}) => ({
	borderRadius: '2rem',
	maxWidth: '15rem',
	color: theme.palette.common.black,
	"& .MuiInputBase-input": {
		color: theme.palette.common.black,
	},
	"& .MuiInputLabel-root": {
		color: theme.palette.common.black,
	}
}))
