import React, {FC, useEffect, useState} from 'react';
import {Box, Button, Card, CardActionArea, Divider, IconButton, Stack, Typography} from "@mui/material";
import ArrowCircleLeftIcon from "@mui/icons-material/ArrowCircleLeft";
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight";
import Grid from "@mui/material/Unstable_Grid2";
import {IActivePlans} from "../../interfaces/IActivePlans";
import PMYoursPlansCard from "./PMYoursPlansCard";
import {useFindUserPlansQuery} from "../../API/API";


const PMYoursPlans: FC = () => {
    const {data} = useFindUserPlansQuery();
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(3)
    const [maxPage, setMaxPage] = useState(1)

    useEffect(() => {
        if(data) setMaxPage(Math.ceil(data.length/rowsPerPage));
    }, [data])

    if(!(data?.length)) {
        return (<></>)
    }

    const handleChangePage = (
        newPage: number,
    ) => {
        if(newPage >= 0 && newPage <= maxPage - 1) {
            setPage(newPage);
        }
    };

    return (
        <>
            <Stack direction={'row'} alignItems={'center'} mb={2}>
                <Typography color={'white'} variant={'h6'}>Ваши инвестиции</Typography>
                <Box>
                    <IconButton onClick={() => handleChangePage(page - 1)}>
                        <ArrowCircleLeftIcon/>
                    </IconButton>
                    <IconButton onClick={() => handleChangePage(page + 1)}>
                        <ArrowCircleRightIcon/>
                    </IconButton>
                </Box>
            </Stack>
            <Grid mb={2} spacing={{xs: 2, md: 3}} columns={{xs: 4, sm: 8, md: 12}} container>
                {
                    data && data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map(activePlan => (
                            <Grid xs={2} sm={4} md={4}>
                                <PMYoursPlansCard data={activePlan} />
                            </Grid>
                        ))
                }
            </Grid>
        </>
    );
};

export default PMYoursPlans;