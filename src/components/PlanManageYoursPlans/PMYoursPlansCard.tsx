import React, {FC, useState} from 'react';
import {Box, Button, Card, CardActionArea, Divider, Stack, Typography} from "@mui/material";
import {EnterInput} from "./styles";
import {IActiveInvesting, IActivePlans} from "../../interfaces/IActivePlans";
import {formatTimeString} from "../../Utils/Utils";
import {useInvestOneMutation} from "../../API/API";


interface IPMYoursPlansCard {
    data: IActiveInvesting;
}


const PMYoursPlansCard: FC<IPMYoursPlansCard> = ({data}) => {

    const [startSummary, setStartSummary] = useState(0)

    const [Invest, {}] = useInvestOneMutation();

    const handleActive = () => {
        Invest({
            id: data.id,
            summary: startSummary,
        });
    }

    const handleChangeStartSummary = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setStartSummary(parseInt(event.target.value, 10))
    }

    const handleOnBlur = (event: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        if(!event.target.value) {
            setStartSummary(0)
        }
    }

    return (
        <Card sx={{ maxWidth: 360, height: "100%" }}>
            <CardActionArea>
                <Box p={4}>
                    <Typography color={'white'} variant={'h6'}>
                        Инвестиционный план
                    </Typography>
                    <Stack mt={2} sx={{gap: '0.2rem'}} direction={'column'}>
                        <Stack justifyContent={'space-between'} direction={'row'}>
                            <Typography variant="body2">
                                Дата начала
                            </Typography>
                            <Typography variant="body2">
                                {formatTimeString(data.plan.startDate).date}
                            </Typography>
                        </Stack>
                        <Divider color={'white'} />
                        <Stack justifyContent={'space-between'} direction={'row'}>
                            <Typography variant="body2">
                                Дата окончания
                            </Typography>
                            <Typography variant="body2">
                                {formatTimeString(data.plan.endDate).date}
                            </Typography>
                        </Stack>
                        <Divider color={'white'} />
                        {
                            data.money && (
                                <>
                                    <Stack justifyContent={'space-between'} direction={'row'}>
                                        <Typography variant="body2">
                                            Стартовый капитал
                                        </Typography>
                                        <Typography variant="body2">
                                            {data.money}
                                        </Typography>
                                    </Stack>
                                    <Divider color={'white'} />
                                </>
                            )
                        }
                        {
                            data.plan.addedFund && (<>
                                <Stack justifyContent={'space-between'} direction={'row'}>
                                    <Typography variant="body2">
                                        Добавленный капитал
                                    </Typography>
                                    <Typography variant="body2">
                                        {data.plan.addedFund}
                                    </Typography>
                                </Stack>
                                <Divider color={'white'} />
                            </>)
                        }
                        {
                            data.plan.financeResult && (<>
                                <Stack justifyContent={'space-between'} direction={'row'}>
                                    <Typography variant="body2">
                                        Финансовый результат
                                    </Typography>
                                    <Typography variant="body2">
                                        {data.plan.financeResult}%
                                    </Typography>
                                </Stack>
                                <Divider color={'white'} />
                            </>)
                        }
                        {
                            data.plan.profit && (<>
                                <Stack justifyContent={'space-between'} direction={'row'}>
                                    <Typography variant="body2">
                                        Прибыль в {data.planCurrency}
                                    </Typography>
                                    <Typography variant="body2">
                                        {data.plan.profit}
                                    </Typography>
                                </Stack>
                                <Divider color={'white'} />
                            </>)
                        }
                        <Stack justifyContent={'space-between'} direction={'row'}>
                            <Typography variant="body2">
                                Статус плана
                            </Typography>
                            <Typography variant="body2">
                                {data.plan.status}
                            </Typography>
                        </Stack>
                        {
                            !data.isActivated && (<>
                                <Divider color={'white'} />
                                <Stack justifyContent={'space-between'} direction={'column'} >
                                    <Typography variant="body2">
                                        Введите сумму стартового капитала
                                    </Typography>
                                    <Stack justifyContent={'space-between'} direction={'column'} >
                                        <Box flex={1}>
                                            <EnterInput value={startSummary} onBlur={handleOnBlur} onChange={handleChangeStartSummary} type="number" min="0" />
                                        </Box>
                                        <Box mt={1} flex={1}>
                                            <Button
                                                variant="contained"
                                                size={'small'}
                                                color="success"
                                                onClick={handleActive}>Активировать</Button>
                                        </Box>
                                    </Stack>
                                </Stack>
                            </>)
                        }
                    </Stack>
                </Box>
            </CardActionArea>
        </Card>
    );
};

export default PMYoursPlansCard;