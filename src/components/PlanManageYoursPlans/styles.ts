import {styled} from "@mui/material";

export const EnterInput = styled('input')(({theme}) => ({
    borderRadius: '0.8rem',
    padding: '0.4rem 0.8rem',
    outline: "none"
}))