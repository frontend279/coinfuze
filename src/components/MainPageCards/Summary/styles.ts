import {Stack, styled} from "@mui/material";

export const SummaryStack = styled(Stack)(({theme}) => ({
    backgroundColor: '#9be3fb',
    height: "100%"
}))