import React, {FC} from 'react';
import {SummaryStack} from "./styles";
import {Box, Divider, Stack, Typography} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import {ISpendInfo} from "../../../interfaces/ISpendInfo";

const Summary:FC<ISpendInfo> = ({info}) => {
    return (
        <Grid xs={2} sm={4} md={4}>
            <SummaryStack direction='column'>
                <Box p={2}>
                    <Typography variant={'h6'}>Сводка</Typography>
                </Box>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Typography variant={'body1'}>Активные инвестиции</Typography>
                    <Typography variant={'body1'}>{info.summary.active}</Typography>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Typography variant={'body1'}>Инвестиционные планы</Typography>
                    <Typography variant={'body1'}>{info.summary.plans}</Typography>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Typography variant={'body1'}>Общая прибыль</Typography>
                    <Typography variant={'body1'}>{info.summary.allSummary}</Typography>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Typography variant={'body1'}>Участник с </Typography>
                    <Typography variant={'body1'}>{info.summary.participationTime}</Typography>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Typography variant={'body1'}>Выведенные средства</Typography>
                    <Typography variant={'body1'}>{info.summary.withdrawnMoney}</Typography>
                </Stack>
                <Divider color='black'/>
            </SummaryStack>
        </Grid>
    );
};

export default Summary;