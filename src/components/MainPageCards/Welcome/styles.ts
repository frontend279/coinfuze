import {Stack, styled} from "@mui/material";

export const WelcomeStack = styled(Stack)(({theme}) => ({
    backgroundColor: '#b9e0f1',
    height: "100%"
}))