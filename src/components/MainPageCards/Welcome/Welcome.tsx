import React from 'react';
import {WelcomeStack} from "./styles";
import {Divider, Typography} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";

const Welcome = () => {
    return (
        <Grid xs={4} sm={8} md={8}>
            <WelcomeStack p={2} gap={1} direction="column">
                <Typography variant={'h6'}>Рады приветствовать вас в CoinFuze!</Typography>
                <Typography variant={'body1'}>Мы создали для вас максимально удобную платформу,
                    чтобы вы могли инвестировать в криптовалюты с комфортом.</Typography>
                <Typography mt={2} variant={'body1'}>Автоматизированная система прибыли</Typography>
                <Divider color={'black'}/>
                <Typography variant={'body1'}>Удобная платформа</Typography>
                <Divider color={'black'}/>
                <Typography variant={'body1'}>Стаильный доход</Typography>
                <Divider color={'black'}/>
                <Typography variant={'body1'}>Полный контроль рисков</Typography>
                <Divider color={'black'}/>
                <Typography variant={'body1'}>Начните инвестировать с CoinFuze</Typography>
                <Divider color={'black'}/>
                <Typography variant={'body1'}>Что делать с доходом</Typography>
                <Divider color={'black'}/>
                <Typography variant={'body1'}>Как увеличить свою прибыль</Typography>
                <Divider color={'black'}/>
            </WelcomeStack>
        </Grid>
    );
};

export default Welcome;