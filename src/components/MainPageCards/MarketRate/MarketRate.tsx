import React, {FC} from 'react';
import {Box, Divider, Stack, Typography} from "@mui/material";
import btc from "../../../assets/img/btc.svg";
import ltc from "../../../assets/img/ltc.svg";
import eth from "../../../assets/img/eth.svg";
import tether from "../../../assets/img/tether.svg";
import Grid from "@mui/material/Unstable_Grid2";
import { MarketRateStack } from './styles';
import {IMarketRate} from "../../../interfaces/IMarketRate";

const MarketRate: FC<IMarketRate> = ({market}) => {
    return (
        <Grid xs={2} sm={4} md={4}>
            <MarketRateStack direction='column'>
                <Box p={2}>
                    <Typography variant={'h6'}>Курсы рынка криптовалют</Typography>
                </Box>
                <Divider color='black'/>
                <Stack p={2} gap={1} direction='row' alignItems='center'>
                    <img src={btc}/>
                    <Stack direction='column'>
                        <Typography variant={'body1'}>Bitcoin (BTC)</Typography>
                        <Typography variant={'body1'}>{market.btc}</Typography>
                    </Stack>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} gap={1} direction='row' alignItems='center'>
                    <img src={ltc}/>
                    <Stack direction='column'>
                        <Typography variant={'body1'}>Litecoin (LTC)</Typography>
                        <Typography variant={'body1'}>{market.ltc}</Typography>
                    </Stack>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} gap={1} direction='row' alignItems='center'>
                    <img src={eth}/>
                    <Stack direction='column'>
                        <Typography variant={'body1'}>Etherium (ETH)</Typography>
                        <Typography variant={'body1'}>{market.eth}</Typography>
                    </Stack>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} gap={1} direction='row' alignItems='center'>
                    <img width="24" height="24" src={tether}/>
                    <Stack direction='column'>
                        <Typography variant={'body1'}>Tether (USDT)</Typography>
                        <Typography variant={'body1'}>{market.usdt}</Typography>
                    </Stack>
                </Stack>
                <Divider color='black'/>
            </MarketRateStack>
        </Grid>
    );
};

export default MarketRate;