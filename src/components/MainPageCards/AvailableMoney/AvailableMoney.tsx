import React, {FC} from 'react';
import {Box, Divider, Stack, Typography} from "@mui/material";
import btc from "../../../assets/img/btc.svg";
import ltc from "../../../assets/img/ltc.svg";
import eth from "../../../assets/img/eth.svg";
import tether from "../../../assets/img/tether.svg";
import Grid from "@mui/material/Unstable_Grid2";
import {AvailableMoneyStack} from "./styles";
import {ISpendInfo} from "../../../interfaces/ISpendInfo";

const AvailableMoney:FC<ISpendInfo> = ({info}) => {
    return (
        <Grid xs={2} sm={4} md={4}>
            <AvailableMoneyStack direction='column'>
                <Box p={2}>
                    <Typography variant={'h6'}>Доступные средства</Typography>
                </Box>
                <Divider color='black'/>
                <Box p={2}>
                    <Typography variant={'h4'}>{info.available.all}</Typography>
                </Box>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Stack direction='row' alignItems='center'>
                        <img src={btc}/>
                        <Typography variant={'body1'}>Bitcoin (BTC)</Typography>
                    </Stack>
                    <Typography variant={'body1'}>{info.available.btc}</Typography>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Stack direction='row' alignItems='center'>
                        <img src={ltc}/>
                        <Typography variant={'body1'}>Litecoin (LTC)</Typography>
                    </Stack>
                    <Typography variant={'body1'}>{info.available.ltc}</Typography>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Stack direction='row' alignItems='center'>
                        <img src={eth}/>
                        <Typography variant={'body1'}>Etherium (ETH)</Typography>
                    </Stack>
                    <Typography variant={'body1'}>{info.available.eth}</Typography>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Stack direction='row' alignItems='center'>
                        <img width="24" height="24" src={tether}/>
                        <Typography variant={'body1'}>Tether (USDT)</Typography>
                    </Stack>
                    <Typography variant={'body1'}>{info.available.usdt}</Typography>
                </Stack>
                <Divider color='black'/>
            </AvailableMoneyStack>
        </Grid>
    );
};

export default AvailableMoney;