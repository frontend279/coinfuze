import {Stack, styled} from "@mui/material";

export const AvailableMoneyStack = styled(Stack)(({theme}) => ({
    backgroundColor: '#b9e0f1',
    height: "100%"
}))