import React, {useEffect, useState} from 'react';
import Grid from "@mui/material/Unstable_Grid2";
import AllMoney from "./AllMoney/AllMoney";
import AvailableMoney from "./AvailableMoney/AvailableMoney";
import InvestedMoney from "./InvestedMoney/InvestedMoney";
import MarketRate from "./MarketRate/MarketRate";
import Summary from "./Summary/Summary";
import News from "./News/News";
import Welcome from "./Welcome/Welcome";
import {ISpendInfo} from "../../interfaces/ISpendInfo";
import {IMarketRate} from "../../interfaces/IMarketRate";

const defaultInfo: ISpendInfo = {
    info: {
        all: {
            all: '28.00$',
            today: '0.00$',
            yesterday: '0.00$',
        },
        available: {
            all: '12.00$',
            btc: '16.00$',
            ltc: '0.00$',
            eth: '0.00$',
            usdt: '0.00$',
        },
        invested: {
            all: '10.00$',
            daySummary: '0.00$',
            weekSummary: '0.00$',
            monthSummary: '0.00$',
            yearSummary: '0.00$',
        },
        summary: {
            active: '0.00$',
            plans: 1,
            allSummary: '0.00$',
            participationTime: '10.20.1022',
            withdrawnMoney: '0.00$',
        }
    }
}

const defaultMarketRate: IMarketRate = {
    market: {
        btc: '$21.65',
        ltc: '$28.65',
        eth: '$28.65',
        usdt: '$28.65',
    }
}

const MainPageCards = () => {

    const [info, setInfo] = useState<ISpendInfo>(defaultInfo)
    const [marketRate, setMarketRate] = useState<IMarketRate>(defaultMarketRate)

    useEffect(() => {
        //TODO: запрос к серверу
    }, [])

    return (
        <Grid spacing={{xs: 2, md: 3}} columns={{xs: 4, sm: 8, md: 12}} container>
            <AllMoney info={info.info}/>
            <AvailableMoney info={info.info}/>
            <InvestedMoney info={info.info}/>
            <Summary info={info.info}/>
            <MarketRate market={marketRate.market}/>
            <News />
            <Welcome />
        </Grid>
    );
};

export default MainPageCards;