import React, {FC} from 'react';
import {Box, Divider, Stack, Typography} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import {InvestedMoneyStack} from "./styles";
import {ISpendInfo} from "../../../interfaces/ISpendInfo";

const InvestedMoney:FC<ISpendInfo> = ({info}) => {
    return (
        <Grid xs={2} sm={4} md={4}>
            <InvestedMoneyStack direction='column'>
                <Box p={2}>
                    <Typography variant={'h6'}>Инвестированные средства</Typography>
                </Box>
                <Divider color='black'/>
                <Box p={2}>
                    <Typography variant={'h4'}>{info.invested.all}</Typography>
                </Box>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Typography variant={'body1'}>Прибыль 24 часа</Typography>
                    <Typography variant={'body1'}>{info.invested.daySummary}</Typography>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Typography variant={'body1'}>Прибыль 7 дней</Typography>
                    <Typography variant={'body1'}>{info.invested.weekSummary}</Typography>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Typography variant={'body1'}>Прибыль 30 дней</Typography>
                    <Typography variant={'body1'}>{info.invested.monthSummary}</Typography>
                </Stack>
                <Divider color='black'/>
                <Stack p={2} justifyContent={'space-between'} direction='row' alignItems='center'>
                    <Typography variant={'body1'}>Прибыль за год</Typography>
                    <Typography variant={'body1'}>{info.invested.yearSummary}</Typography>
                </Stack>
                <Divider color='black'/>
            </InvestedMoneyStack>
        </Grid>
    );
};

export default InvestedMoney;