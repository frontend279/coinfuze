import {Stack, styled} from "@mui/material";

export const InvestedMoneyStack = styled(Stack)(({theme}) => ({
    backgroundColor: '#b0f2ce',
    height: "100%"
}))