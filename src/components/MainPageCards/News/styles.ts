import {Stack, styled} from "@mui/material";

export const NewsStack = styled(Stack)(({theme}) => ({
    backgroundColor: '#d5ecf4',
    height: "100%"
}))