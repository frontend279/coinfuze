import React, {FC} from 'react';
import {NewsStack} from "./styles";
import {Box, Button, Divider, Stack, Typography} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import {ISpendInfo} from "../../../interfaces/ISpendInfo";
import {useNavigate} from "react-router-dom";

const News:FC = () => {

    let navigate = useNavigate();

    const buttonHandler = () => {
        navigate('/news');
    }

    return (
        <Grid xs={2} sm={4} md={4}>
            <NewsStack direction='column'>
                <Box p={2}>
                    <Typography variant={'h6'}>Новости</Typography>
                </Box>
                <Divider color='black'/>
                <Stack gap={1} p={3} direction='column'>
                    <Typography variant={'body2'}>
                        18.423.432
                    </Typography>
                    <Typography variant={'h6'}>Strripe в сотрудничестве с Twitter вновь займется развитием
                        криптоплатежей</Typography>
                    <Typography variant={'body1'}>Платежные сервис Стьрайп в сотднуриекик с епгшрпи вновть
                        заейтмся развитием крпитовалюттеый один из самых дорогихз стартапов в мире позволит
                        пользователям твиттер получить да свой...</Typography>
                    <Button variant="contained"
                            size={'medium'}
                            sx={{borderRadius: '20px', alignSelf: 'start', padding: '0.5rem 1rem'}}
                            color={'success'}
                    onClick={buttonHandler}>
                        Читать все новости
                    </Button>
                </Stack>
            </NewsStack>
        </Grid>
    );
};

export default News;