import React, {FC, useEffect, useState} from 'react';
import {Box, Divider, Typography} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import {AllMoneyStack} from "./styles";
import {ISpendInfo} from "../../../interfaces/ISpendInfo";

const AllMoney:FC<ISpendInfo> = ({info}) => {

    return (
        <Grid xs={2} sm={4} md={4}>
            <AllMoneyStack direction={'column'}>
                <Box p={2}>
                    <Typography variant={'h6'}>Всего средств</Typography>
                </Box>
                <Divider color='black'/>
                <Box p={2}>
                    <Typography variant={'h4'}>{info.all.all}</Typography>
                </Box>
                <Divider color='black'/>
                <Box p={2}>
                    <Typography variant={'h6'}>{info.all.today}</Typography>
                    <Typography variant={'body1'}>Прибыль сегодня</Typography>
                </Box>
                <Divider color='black'/>
                <Box p={2}>
                    <Typography variant={'h6'}>{info.all.yesterday}</Typography>
                    <Typography variant={'body1'}>Прибыль вчера</Typography>
                </Box>
                <Divider color='black'/>
            </AllMoneyStack>
        </Grid>
    );
};

export default AllMoney;