import {Stack, styled} from "@mui/material";

export const AllMoneyStack = styled(Stack)(({theme}) => ({
    backgroundColor: '#d5ecf4',
    height: "100%"
}))