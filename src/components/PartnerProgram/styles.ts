import {Stack, styled} from "@mui/material";

export const RefProgramStack = styled(Stack)({
    backgroundColor: '#ceedf5',
    padding: '1rem 2rem'
})