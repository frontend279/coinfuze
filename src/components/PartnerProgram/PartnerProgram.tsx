import React, {useState} from 'react';
import {Box, Button, Divider, Stack, Typography} from "@mui/material";
import {RefProgramStack} from "./styles";

interface IStats {
    allInvited: number;
    firstLvLPartners: number;
    secondLvLPartners: number;
    thirdLvLPartners: number;
}

const PartnerProgram = () => {

    const [stats, setStats] = useState<IStats>({
        allInvited: 0,
        firstLvLPartners: 0,
        secondLvLPartners: 0,
        thirdLvLPartners: 0,
    })

    const aboutRefsButtonHandler = () => {
        console.log('aboutRefsButtonHandler')
    }

    return (
        <RefProgramStack flex={1} gap={2} direction={'column'}>
            <Stack gap={1} justifyContent={'space-between'} alignItems={'center'} direction={'row'}>
                <Typography variant={'h5'}>Реферальная программа</Typography>
                <Button variant="contained"
                        size={'large'}
                        sx={{borderRadius: '20px', border: '2px solid black', maxWidth: '200px'}}
                        color={'success'}
                        onClick={aboutRefsButtonHandler}>Подробнее</Button>
            </Stack>
            <Divider color={'black'} />
            <Box flex={1}>
                <Typography variant={'body1'}>Рекомендуйте нас друзьям и получайте бонусы на ваш личный счет.</Typography>
                <Typography variant={'body1'}>Получайте пассивный доход по реферальной программе.</Typography>
                <br />
                <Typography variant={'body2'}>• Скопируйте свою реферальную ссылку</Typography>
                <Typography variant={'body2'}>• Отправьте друзьям</Typography>
                <Typography variant={'body2'}>• Получайте бонусы за их пополнение депозита</Typography>
                <Typography variant={'body2'}>• Получайте бонусы от их прибыли</Typography>
                <Typography variant={'body2'}>• Выводите доход или реинвестируйте что бы еще больше увеличить ваш доход</Typography>
            </Box>
            <Divider color={'black'} />
            <Stack  justifyContent={'space-around'} gap={1} direction={'row'}>
                <Stack direction={'column'}>
                    <Typography variant={'body1'}>Всего приглашено</Typography>
                    <Typography variant={'body1'}>{stats.allInvited}</Typography>
                </Stack>
                <Divider color={'black'} orientation={'vertical'}/>
                <Stack direction={'column'}>
                    <Typography variant={'body1'}>Партнеры 1 линии</Typography>
                    <Typography variant={'body1'}>{stats.firstLvLPartners}</Typography>
                </Stack>
                <Divider color={'black'} orientation={'vertical'}/>
                <Stack direction={'column'}>
                    <Typography variant={'body1'}>Партнеры 2 линии</Typography>
                    <Typography variant={'body1'}>{stats.secondLvLPartners}</Typography>
                </Stack>
                <Divider color={'black'} orientation={'vertical'}/>
                <Stack direction={'column'}>
                    <Typography variant={'body1'}>Партнеры 3 линии</Typography>
                    <Typography variant={'body1'}>{stats.thirdLvLPartners}</Typography>
                </Stack>
            </Stack>
        </RefProgramStack>
    );
};

export default PartnerProgram;