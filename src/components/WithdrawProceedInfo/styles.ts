import {Alert, styled} from "@mui/material";

export const InfoAlert = styled(Alert)(({theme}) => ({
    display: "flex",
    alignItems: "center",
    marginTop: '3rem',
    color: theme.palette.common.black,
    background: "linear-gradient(90deg, rgba(0,219,166,1) 2%, rgba(0,202,181,1) 7%, rgba(0,180,199,1) 52%, rgba(78,160,215,1) 76%, rgba(77,160,215,1) 94%, rgba(86,157,217,1) 95%)"
}))