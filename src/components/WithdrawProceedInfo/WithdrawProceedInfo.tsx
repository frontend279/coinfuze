import React from 'react';
import {Typography} from "@mui/material";
import {InfoAlert} from "./styles";

const WithdrawProceedInfo = () => {
    return (
        <InfoAlert severity={'error'}>
            <Typography variant={'h6'}>Информация</Typography>
            <Typography>Показанный баланс - это ваш доступный баланс, а не общая сумма средств. Если
                у вас есть активные инвестиции,они не учитываются, потому что они заблокированы до окончания
                инвестиции</Typography>
        </InfoAlert>
    );
};

export default WithdrawProceedInfo;