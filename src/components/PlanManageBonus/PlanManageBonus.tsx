import React from 'react';
import {Divider, Stack, Typography} from "@mui/material";
import {InfoBonusBox, InfoStack} from "./styles";

const PlanManageBonus = () => {
    return (
        <InfoStack p={3} direction={'row'}>
            <Stack flex={2} direction={'column'}>
                <Typography variant={'h6'}>Управление инвестициями</Typography>
                <Typography variant={'body1'}>
                    Управление своими инвестиционными планами в CoinFuze
                </Typography>
                <br />
                <Typography variant={'body1'}>
                    На этой странице мы собрали для вас инвестициооные планы, которыми вы можете
                    с легкостью управлять.
                </Typography>
                <br />
                <Typography variant={'body1'}>
                    Заходите в личный кабинет и наблюдайте, как день за днем растет ваша прибыль.
                </Typography>
                <br />
                <Typography variant={'body1'}>
                    Совет от CoinFuze. Используйте функцию пополненич баланса под вашими инвестиционными
                    планами, чтобы добавить к плану дополнительный
                    капитал и получать бонус на всю вашу прибыль
                </Typography>
            </Stack>
            <Stack flex={1} direction={'column'}>
                <Stack p={1} direction={'row'} justifyContent={'space-between'}>
                    <Typography variant={'h6'}>Бонусы за инвестиции</Typography>
                </Stack>
                <Divider color={'black'} />
                <Stack p={1} direction={'row'} justifyContent={'space-between'}>
                    <Typography variant={'body1'}>от 10 до 5000$</Typography>
                    <Typography variant={'body1'}>без бонуса</Typography>
                </Stack>
                <Divider color={'black'} />
                <Stack p={1} gap={1} direction={'row'} justifyContent={'space-between'}>
                    <Typography variant={'body1'}>от 5001 до 10000$</Typography>
                    <Typography variant={'body1'}>10%</Typography>
                </Stack>
                <Divider color={'black'} />
                <Stack p={1} gap={1} direction={'row'} justifyContent={'space-between'}>
                    <Typography variant={'body1'}>от 10001 до 50000$</Typography>
                    <Typography variant={'body1'}>15%</Typography>
                </Stack>
                <Divider color={'black'} />
                <Stack p={1} gap={1} direction={'row'} justifyContent={'space-between'}>
                    <Typography variant={'body1'}>от 50001 до 500000$</Typography>
                    <Typography variant={'body1'}>25%</Typography>
                </Stack>
                <Divider color={'black'} />
                <InfoBonusBox>
                    <Typography variant={'body1'}>Бонус автопилота</Typography>
                    <Typography variant={'body1'}>+1% за завершенный</Typography>
                    <Typography variant={'body1'}>инвестиционный срок</Typography>
                    <Typography variant={'body1'}>Максимум +20%</Typography>
                </InfoBonusBox>
            </Stack>
        </InfoStack>
    );
};

export default PlanManageBonus;