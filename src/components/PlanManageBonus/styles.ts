import {Box, Stack, styled} from "@mui/material";

export const InfoStack = styled(Stack)(({theme}) => ({
    backgroundColor: '#ceedf5'
}))

export const InfoBonusBox = styled(Box)(({theme}) => ({
    backgroundColor: '#afcad1',
    padding: '0.7rem 2rem',
    marginTop: '1rem',
    textAlign: 'center'
}))