import React from 'react';
import {Box, Button, Typography} from "@mui/material";
import {WithdrawStack} from "./styles";
import {useNavigate} from "react-router-dom";

const WithdrawReinvest = () => {

    const navigate = useNavigate()

    const buttonHandler = () => {
        navigate('/deposit/proceed')
    }

    return (
        <WithdrawStack flex={1} color={'black'} direction={'column'} p={2}>
            <Typography variant={'h5'}>Не упустите свой шанс получить больше прибыли!</Typography>
            <br />
            <Typography>Вы можете получить еще больше прибыли, реинвестируя средства</Typography>
            <br />
            <Typography>Реинвестируйте депозит и полученную прибыль</Typography>
            <Typography>Пройдите инвестиционный период</Typography>
            <Typography>Наблюдайте ежедневно, как увеличивается ваш доход</Typography>
            <Box flex={1} alignItems="end" display={'flex'}>
                <Button
                    variant="contained"
                    size={'large'}
                    sx={{borderRadius: '1rem', maxWidth: '300px', marginTop: '2rem'}}
                    color="success"
                    onClick={buttonHandler}>Реинвестировать сейчас</Button>
            </Box>
        </WithdrawStack>
    );
};

export default WithdrawReinvest;