import React, {useState} from 'react';
import {HistoryStack, HistoryTableCell, HistoryTablePagination} from "./styles";
import {Divider, Table, TableBody, TableHead, TableRow, Typography} from "@mui/material";

interface IWithdrawRows {
    date: string
    currency: string
    summary: string
    comment: string
    status: string
}

const WithdrawTable = () => {

    const [historyRows, setHistoryRows] = useState<IWithdrawRows[]>([{
        date: '2023.23.04',
        currency: 'TETHER',
        summary: '300.00$',
        comment: 'Завершен',
        status: 'OK'
    }, {
        date: '2023.23.04',
        currency: 'TETHER',
        summary: '300.00$',
        comment: 'Завершен',
        status: 'OK'
    }])
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);


    const handleChangePage = (
        event: React.MouseEvent<HTMLButtonElement> | null,
        newPage: number,
    ) => {
        setPage(newPage);
    };
    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    return (
        <HistoryStack  p={2} direction={'column'} mt={3}>
            <Typography mb={3} variant={'h5'}>История</Typography>
            <Divider color={'black'} />
            <Table sx={{ minWidth: 650 }}>
                <TableHead >
                    <TableRow >
                        <HistoryTableCell align="center">Дата</HistoryTableCell>
                        <HistoryTableCell align="center">Валюта вывода</HistoryTableCell>
                        <HistoryTableCell align="center">Сумма вывода</HistoryTableCell>
                        <HistoryTableCell align="center">Комментарий</HistoryTableCell>
                        <HistoryTableCell align="center">Статус транзакции</HistoryTableCell>
                    </TableRow>
                </TableHead>
                {
                    historyRows.map(row => (
                        <TableBody>
                        <HistoryTableCell align="center">{row.date}</HistoryTableCell>
                        <HistoryTableCell align="center">{row.currency}</HistoryTableCell>
                        <HistoryTableCell align="center">{row.summary}</HistoryTableCell>
                        <HistoryTableCell align="center">{row.comment}</HistoryTableCell>
                        <HistoryTableCell align="center">{row.status}</HistoryTableCell>
                    </TableBody>))
                }
            </Table>
            <HistoryTablePagination
                sx={{color: 'white'}}
                rowsPerPageOptions={[5, 10, 15]}
                count={historyRows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                labelRowsPerPage={'Кол-во строк:'}
            />
        </HistoryStack>
    );
};

export default WithdrawTable;