import React, {FC} from 'react';
import {Button, Stack, Typography} from "@mui/material";
import {InfoAlert} from "./styles";
import {useDispatch} from "react-redux";
import {CompleteRegActions} from "../../redux/slices/others/CompleteRegSlices";
import {UserDataActions} from "../../redux/slices/user/UserDataSlice";

const CompleteRegisterAlert: FC<ICompleteRegisterAlert> = ({isShown}) => {

    const dispatch = useDispatch()
    const CompleteButtonHandler = () => {
        // Здесь рефка на запросы, но я не придумал какие
        dispatch(CompleteRegActions.setShown(false))
        dispatch(UserDataActions.setData(""))
    }

    if(!isShown) {
        return (<div></div>)
    } else {
        return (
            <InfoAlert severity={'error'}>
                <Stack direction={'row'} justifyContent={'space-between'}>
                    <Stack flex={1} direction={'column'}>
                        <Typography variant={'h6'}>Ваша регистрация почти завершена</Typography>
                        <Typography>Перед инвестированием необходимо завершить процесс регистрации, заполнив свои
                            личные данные.</Typography>
                    </Stack>
                    <Button variant="contained"
                            size={'medium'}
                            sx={{borderRadius: '20px', alignSelf: 'center', padding: '0.5rem 1rem'}}
                            color={'success'}
                            onClick={CompleteButtonHandler}>
                        Завершить регистрацию
                    </Button>
                </Stack>
            </InfoAlert>
        );
    }
};

interface ICompleteRegisterAlert {
    isShown: boolean
}

export default CompleteRegisterAlert;