import React, {FC, useEffect, useState} from 'react';
import {Stack, Typography} from "@mui/material";

interface IPageHead {
    title: string;
}

const PageHead: FC<IPageHead> = ({title}) => {

    useEffect(() => {
        //TODO: достаем время из юзера
        setDate('18.10.2024')
    }, [])

    const [date, setDate] = useState<string>('18.10.2023')

    return (
        <Stack color={'white'} direction={'row'} alignItems={'center'} justifyContent={'space-between'}>
            <Typography variant={'h5'}>{title}</Typography>
            <Typography variant={'body1'}>Последний вход 23.04.2023</Typography>
        </Stack>
    );
};

export default PageHead;