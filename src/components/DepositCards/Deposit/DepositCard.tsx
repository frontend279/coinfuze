import React from 'react';
import {Box, Button, Typography} from "@mui/material";
import {WithdrawStack} from "../styles";
import {useNavigate} from "react-router-dom";

const DepositCard = () => {

    const navigate = useNavigate()

    const buttonHandler = () => {
        navigate('/deposit/proceed')
    }

    return (
        <WithdrawStack flex={1} color={'black'} direction={'column'} p={2}>
            <Typography variant={'h5'}>Внесите депозит на свой счет и начните зарабатывать прямо сейчас</Typography>
            <br/>
            <Typography>Специалисты CoinFuze более трех лет работают в области алгоритмического трейдинга и создали
                Партнерская программа автоматизированную систему генерации прибыли</Typography>
            <Typography>Торговля криптовалютами имеет ряд преимуществ, которые мы можем использовать для получения Годовая
                прибыль — 107,7%</Typography>
            <br/>
            <Typography>• Высокая волатильность активов — зарабатывать можно как на росте, таки на спаде
                рынка</Typography>
            <Typography>• Кругосуточная торговля — криптовалютный рынок работает 24/7, потому мы можем беспрерывно
                личная информация генерировать для вас прибыль</Typography>
            <Typography>• Отсутствие зависимости от банковской системы — вы пополняете счет криптовалютой и выводите
                выводите прибыль на криптовалютный кошелек</Typography>
            <Typography>• Высокая динамика - за короткий период возможно получить высокую прибыль</Typography>
            <Box flex={1} alignItems="end" display={'flex'}>
                <Button
                    variant="contained"
                    size={'large'}
                    sx={{borderRadius: '1rem', maxWidth: '300px', marginTop: '2rem'}}
                    color="success"
                    onClick={buttonHandler}>Пополнить депозит</Button>
            </Box>
        </WithdrawStack>
    );
};

export default DepositCard;