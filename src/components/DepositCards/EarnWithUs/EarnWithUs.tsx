import React from 'react';
import {Typography} from "@mui/material";
import {WithdrawStack, EarnCustomBox} from "../styles";

const EarnWithUs = () => {
    return (
        <WithdrawStack flex={1} color={'black'} direction={'column'} p={2}>
            <Typography variant={'h5'}>Зарабатывайте вместе с нами</Typography>
            <br/>
            <EarnCustomBox>
                <Typography>Ежемесячная прибыль - 8.5%</Typography>
            </EarnCustomBox>
            <br/>
            <EarnCustomBox>
                <Typography>Годовая прибыль - 107.7%</Typography>
            </EarnCustomBox>
            <br/>
            <EarnCustomBox>
                <Typography>Полностью автоматизированная система</Typography>
            </EarnCustomBox>
            <br/>
            <EarnCustomBox>
                <Typography>Возможность реинвестирования</Typography>
            </EarnCustomBox>
            <br/>
            <Typography>После пополнения депозита вы сможете пользоваться всеми преимуществами сотрудничества с CoinFuze</Typography>

        </WithdrawStack>
    );
};

export default EarnWithUs;