import {Box, Stack, styled} from "@mui/material";

export const WithdrawStack = styled(Stack)({
    backgroundColor: 'rgb(206,237,245)'
})

export const EarnCustomBox = styled(Box)({
    backgroundColor: '#a9d2dd',
    borderRadius: '2rem',
    padding: '0.5rem 1rem'
})