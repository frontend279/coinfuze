import React, {useEffect, useState} from 'react';
import {Box, CardActionArea, CardMedia, Divider, IconButton, Stack, Typography} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2";
import PMAvailableCard from "./PMAvailableCard";
import ArrowCircleLeftIcon from "@mui/icons-material/ArrowCircleLeft";
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight";
import {useFindAllAvailableQuery} from "../../API/API";


const PMAvailable = () => {



    const {data, isLoading, error} =  useFindAllAvailableQuery()

    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(3)
    const [maxPage, setMaxPage] = useState(1)

    useEffect(() => {
        if(data) setMaxPage(Math.ceil(data.length/rowsPerPage));
    }, [data, ])

    if(!data?.length) {
        return (<></>)
    }

    const handleChangePage = (
        newPage: number,
    ) => {
        if(newPage >= 0 && newPage <= maxPage - 1) {
            setPage(newPage);
        }
    };

    return (
        <>
            <Stack direction={'row'} alignItems={'center'} mb={2}>
                <Typography color={'white'} variant={'h4'}>Доступные планы</Typography>
                <Box>
                    <IconButton onClick={() => handleChangePage(page - 1)}>
                        <ArrowCircleLeftIcon/>
                    </IconButton>
                    <IconButton onClick={() => handleChangePage(page + 1)}>
                        <ArrowCircleRightIcon/>
                    </IconButton>
                </Box>
            </Stack>
            <Grid spacing={{xs: 2, md: 3}} columns={{xs: 4, sm: 8, md: 12}} container>
                {data && data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((item, index) => (
                        <Grid key={index} xs={2} sm={4} md={4}>
                            <PMAvailableCard plan={item}/>
                        </Grid>
                    ))}

            </Grid>
        </>
    );
};

export default PMAvailable;