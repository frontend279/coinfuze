import React, {FC} from 'react';
import {Box, CardActionArea, CardMedia, Divider, Stack, Typography} from "@mui/material";
import {PlanCard} from "./styles";
import {IAvailablePlan} from "../../interfaces/IActivePlans";
import {getTimeDiff} from "../../Utils/Utils";
import {usePickPlanMutation} from "../../API/API";

interface IPMAvailableCard {
    plan: IAvailablePlan;
}

const PMAvailableCard: FC<IPMAvailableCard> = ({plan}) => {

    const [PickOne, {data}] = usePickPlanMutation();


    const handleSubmit = () => {
        PickOne({id: plan.id});
    }

    return (
        <PlanCard sx={{ maxWidth: 345 }} onClick={handleSubmit}>
            <CardActionArea>
                <Box style={{padding: '1rem'}}>
                    <CardMedia
                        component="img"
                        height="140"
                        image={""}
                        alt={'invest'}
                    />
                </Box>
                <Stack direction={'column'} gap={1} p={2}>
                    <Stack justifyContent={'space-between'} direction={'row'}>
                        <Typography variant="body2">
                            Мин. инвестиции
                        </Typography>
                        <Typography variant="body2">
                            {plan.minInvest}
                        </Typography>
                    </Stack>
                    <Divider color={'white'} />
                    <Stack justifyContent={'space-between'} direction={'row'}>
                        <Typography variant="body2">
                            Срок
                        </Typography>
                        <Typography variant="body2">
                            {getTimeDiff(Number(plan.startDate), Number(plan.endDate)).days} дней
                        </Typography>
                    </Stack>
                    <Divider color={'white'} />
                    <Stack justifyContent={'space-between'} direction={'row'}>
                        <Typography variant="body2">
                            Доходность
                        </Typography>
                        <Typography variant="body2">
                            {plan.waitingFinanceResult}
                        </Typography>
                    </Stack>
                    <Divider color={'white'} />
                </Stack>
            </CardActionArea>
        </PlanCard>
    );
};

export default PMAvailableCard;