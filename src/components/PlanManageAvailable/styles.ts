import {Card, styled} from "@mui/material";

export const PlanCard = styled(Card)(({theme}) => ({
    backgroundColor: '#1e1e20',
    "&:hover": {
        backgroundColor: '#072d2a'
    }
}))