import {BrowserRouter} from "react-router-dom";
import {Router} from "./pages/PagesConsts";
import {Box, Stack} from "@mui/material";
import LeftMenu from "./components/LeftMenu/LeftMenu";
import React, {useState} from "react";
import Footbar from "./components/Footbar/Footbar";
import AuthModal from "./components/modals/AuthModal/AuthModal";
import AdminLeftMenu from "./components/LeftMenu/AdminLeftMenu";
import Navbar from "./components/Navbar/Navbar";

function App() {

    const [isAdmin, setIsAdmin] = useState(false)

    return (
        <Box>
            <BrowserRouter>
                <Navbar/>
                <Stack direction="row">
                    {
                        isAdmin ? <AdminLeftMenu setIsAdmin={setIsAdmin} /> : <LeftMenu setIsAdmin={setIsAdmin} />
                    }
                    <Stack flex={1} direction="column">
                        <Router/>
                        {
                            isAdmin ? <></> : <Footbar/>
                        }
                    </Stack>
                </Stack>
            </BrowserRouter>
            <AuthModal />
        </Box>
    )
}

export default App
