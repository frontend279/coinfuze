import {baseURL} from "../API/API";

export const getAvatarURL = (avatarName: string) => {
    return baseURL + "/avatars/" + avatarName;
}

export const formatTimeFromDatabase = (time: string) => {

    const date = new Date(time);

    return {
        date:
            ('0' + date.getDate()).slice(-2) +
            '.' +
            ('0' + (date.getMonth() + 1)).slice(-2) +
            '.' +
            date.getFullYear(),
        time: date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds(),
    };
};

export const formatTimeString = (date: string) => {
    const time = new Date();
    time.setTime(+date);
    return {
        date:
            ('0' + time.getDate()).slice(-2) +
            '.' +
            ('0' + (time.getMonth() + 1)).slice(-2) +
            '.' +
            time.getFullYear(),
        time: time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds(),
    };
};

export const getTimeDiff = (startDate: number, endDate: number) => {
    const diff = endDate - startDate;
    const days = Math.floor(diff / 1000 / 60 / (60*24));
    return {
        days,
    }
}